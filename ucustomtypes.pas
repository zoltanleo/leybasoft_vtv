unit ucustomtypes;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils;

type

  //режим вызова модального окна: добавление/редактирование записи
  TEditMode = (emAdd, emEdit);

  //структура узла
  PTreeData = ^TTreeData;
  TTreeData = record
    StrCaption: String;//короткое наименование
    StrDescript: String;//подробное наименование
    ID: PtrInt;
    ParentID: PtrInt;
    OwnerUser: String;//владелец-юзер
    OwnerRole: String;//владелец-роль
  end;

  //опции поиска узлов
  TNodeSearchOption = (
      nsoSelectAllNodes      //выделять все узлы
    , nsoSearchCaseSensitive //поиск с учетом регистра
                      );
  TNodeSearchOptions = set of TNodeSearchOption;

  //поле в структуре узла, по которому осуществляется поиск
  TSearchDataField = (
          sdfCaption  //по StrCaption
        , sdfDescript //по StrDescript
                      );

  //направления поиска узлов в дереве
  TNodeSearchDirection = (
              nsdSearchDown  //поиск по дереву сверху вниз
            , nsdSearchUp    //поиск по дереву снизу вверх
                          );

implementation

end.

