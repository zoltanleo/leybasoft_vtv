unit ufredit;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, StdCtrls, LCLType, LMessages, LCLIntf,
  ExtCtrls;

type

  { TfrEdit }

  TfrEdit = class(TFrame)
    bvlFrame: TBevel;
    btnLeft: TButton;
    btnRigth: TButton;
    edtEdit: TEdit;
    lblDescription: TLabel;
    lblCaption: TLabel;
    memoEdit: TMemo;
  private
    FAnchoringTopControl: TControl;
    FChildID: PtrInt;//ID узла, отображаемого в фрейме
    FIsAddMode: Boolean;
    procedure CMShowingChanged(var msg: TLMessage); message CM_SHOWINGCHANGED;
  public
    property IsAddMode: Boolean read FIsAddMode write FIsAddMode;//фрейм создан в режиме добавления записей
    constructor Create(TheOwner: TComponent); override;
    destructor Destroy; override;
    property AnchoringTopControl: TControl read FAnchoringTopControl write FAnchoringTopControl;
    property ChildID: PtrInt read FChildID write FChildID;//ID узла, отображаемого в фрейме
  end;

implementation

uses Unit1;

{$R *.lfm}

{ TfrEdit }

procedure TfrEdit.CMShowingChanged(var msg: TLMessage);
begin
  inherited;
  if Showing then
    begin
      // Show
      btnLeft.Visible:= IsAddMode;
      btnRigth.Visible:= IsAddMode;

      Self.AnchorSideLeft.Control:= Parent;
      Self.AnchorSideLeft.Side:= asrLeft;
      Self.AnchorSideRight.Control:= Parent;
      Self.AnchorSideRight.Side:= asrBottom;
      Self.AnchorSideTop.Control:= AnchoringTopControl;

      Self.Anchors:= [akLeft,akTop, akRight];

    end
  else
    begin
      // Hide
    end;
end;

constructor TfrEdit.Create(TheOwner: TComponent);
begin
  inherited Create(TheOwner);
  Self.Name:= 'fr_' + FormatDateTime('yymmdd_hhmmsszzz', Now);
  Self.AutoSize:= True;
  FIsAddMode:= True;//режим добавления записей (по умолчанию)
  btnLeft.Width:= Form1.btnRootAdd.Width;
  btnLeft.Height:= Form1.btnRootAdd.Height;
  btnRigth.Width:= Form1.btnRootAdd.Width;
  btnRigth.Height:= Form1.btnRootAdd.Height;
  {$IFDEF MSWINDOWS}
  btnLeft.OnClick:= @Form1.ActFrameCreateExecute;
  btnRigth.OnClick:= @Form1.ActFrameRemoveExecute;
  btnLeft.Caption:= '+';
  btnRigth.Caption:= '-';
  {$ELSE}
  btnLeft.OnClick:= @Form1.ActFrameRemoveExecute;
  btnRigth.OnClick:= @Form1.ActFrameCreateExecute;
  btnLeft.Caption:= '-';
  btnRigth.Caption:= '+';
  {$ENDIF}

  FChildID:= -1;
  edtEdit.Clear;
  memoEdit.Clear;
end;

destructor TfrEdit.Destroy;
begin
  inherited Destroy;
end;

end.

