unit uresourcestring;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils;

resourcestring
  {$REGION 'блок констант для локализации программы'}
  CaptDragZoneDock = 'Дважды щелкните здесь' +
                    {$IFNDEF DARWIN}
                    ' или немного потащите форму в любую сторону в пределах этого прямоугольника' +
                    {$ENDIF}
                    ', чтобы пристыковать окно';
  CaptDragZoneUnDock = 'Дважды щелкните здесь' +
                      {$IFNDEF DARWIN}
                      ' или немного потащите форму в любую сторону в пределах этого прямоугольника' +
                      {$ENDIF}
                      ', чтобы отстыковать окно';

  CaptWarningDrugAllergy = 'Укажите здесь имеющуюся лекарственную непереносимость у пациента';
  CaptWarningPhones = 'Добавьте номера телефонов пациента';
  CaptWarningMedPolice = 'Добавьте страховой полис пациента';
  CaptWarningIdentif = 'Добавьте какой-либо документ, удостоверяющий личность пациента';
  CaptWarningMainPatInfoPatNo = 'Нет открытых вкладок с пациентами';
  CaptWarningMainPatInfoPatYes = 'Сюда можно пристыковать открытые окна с данными пациентов';
  CaptWarningVisits = 'Пациент(ка) визитов в ЛПУ не имеет';


  CaptLblErrorStr = 'Ошибка загрузки данных';
  CaptLblDefaultStr = 'нет данных';
  CaptLblDateFormatDMYStr = 'dd.mm.yyyy';
  CaptLblCurrUnit = 'руб.';
  CaptLblMobile = 'Код мобильного оператора';
  CaptLblLocate = 'Код населенного пункта';

  CaptEditWindowtsPassData = 'паспортные данные';
  CaptEditWindowtsPhones = 'телефоны';
  CaptEditWindowtsResidence = 'место жительства/регистрации';
  CaptEditWindowtsMedPolice = 'мед.полис';
  CaptEditWindowtsIdentification = 'удостоверение личности';
  CaptEditWindowtsDrugAllergy = 'лекарственная непереностимость';
  CaptEditWindowtsVisits = 'данные визита в ЛПУ';

  CaptcbbItemAllRegion = '<все регионы>';
  CaptcbbItemOMS = 'ОМС';
  CaptcbbItemDMS = 'ДМС';
  CaptcbbItemStatusInvalid = 'недействительный';
  CaptcbbItemStatusValidUntilExp = 'действительный до окончания срока';

  CaptcbbItemDocTypeDriverLic = 'Водительское удостоверение';
  CaptcbbItemDocTypeSocialNum = 'СНИЛС';
  CaptcbbItemDocTypePasspInternal = 'Паспорт гражданина';
  CaptcbbItemDocTypePasspInternational = 'Загран.паспорт';
  CaptcbbItemDocTypeResidentCard = 'Вид на жительство';
  CaptcbbItemDocTypeWorkPermit = 'Разрешение на работу';
  CaptcbbItemDocTypeForeignStudent = 'Разрешение на учебу(студент)';
  CaptcbbItemDocTypeMilitaryID = 'Военный билет';
  CaptcbbItemDocTypePasspDiplomatic = 'Дипломатический паспорт';

  CaptcbbItemCountryRus = 'Российская Федерация';
  CaptcbbItemCountryUkr = 'Украина';
  CaptcbbItemCountryKaz = 'Казахстан';
  CaptcbbItemCountryKgz = 'Кыргызстан';
  CaptcbbItemCountryUzb = 'Узбекистан';
  CaptcbbItemCountryTjk = 'Таджикистан';
  CaptcbbItemCountryMda = 'Молдова';
  CaptcbbItemCountryChn = 'Китай';

  MsgCaptErrorConnectDB = 'Ошибка коннекта к базе данных';
  MsgCaptErrorReadData = 'Ошибка чтения данных';
  MsgCaptErrorModifyData = 'Ошибка модификации данных';
  MsgCaptErrorFrameCreating = 'Ошибка создания фрейма';
  MsgCaptErrorFrameDeleting = 'Ошибка удаления фрейма';
  MsgCaptNotEnoughData = 'Недостаточно данных';
  MsgCaptUndoChanges = 'Отмена изменений';
  MsgCaptInvalidData ='Некорректные данные';
  MsgCaptWarningData ='Предупреждение';
  MsgCaptErrorAccessDenied = 'Нарушение прав доступа';

  MsgTextControlErrorDetail = 'Ошибка: "%s" %s модуль: "%s" %s компонент: "%s"';
  MsgTextUndoRequest ='Вы уверены, что хотите отменить все внесенные изменения?';
  MsgTextTypedValueIsNotNumber = 'Введенное значение "%s" не является числом';
  MsgTextTblInsurCompIsEmpty = 'Таблица с данными страховых компаний пуста';
  MsgTextTblDrugNameIsEmpty = 'Таблица с лекарственными средствами пуста';
  MsgTextTblDrugReactionIsEmpty = 'Таблица с видами аллергических реакций пуста';
  MsgTextFieldCannotBeEmpty = 'Поле "%s" не может быть пустым';
  MsgTextInvalidPhoneCode = 'Введенный код не соответствует ни одной стране мира в вашей ' +
                                      'базе данных. Для выбора корректного значения Вы можете ' +
                                      'воспользоваться справочником.';
  MsgTextInvalidCountryPhoneCode = 'Для выбранной страны населенного пункта с ' +
                                      'таким кодом в вашей базе данных не найдено. ' +
                                      'Для выбора корректного значения попробуйте ' +
                                      'воспользоваться встроенным справочником.';
  MsgTextCountryCodePrompt = 'Вы не ввели код страны. Для выбора корректного значения ' +
                                  'Вы можете воспользоваться встроенным справочником.';
  MsgTextCountryCodeNotProvided = 'Не указан код страны';
  MsgTextAreaCodeNotProvided = 'Не указан код населенного пункта';
  MsgTextPhoneNumberNotProvided = 'Не указан телефонный номер';

  CaptHintInsurCompName = 'Наименование СК';
  CaptHintInsurCompLicense = 'Лицензия';
  CaptHintInsurCompAddress = 'Адрес';
  CaptHintInsurCompPhones = 'Телефон(ы)';
  CaptHintInsurCompEMail = 'e-mail';
  CaptHintInsurCompComments = 'Kомментарии';

  CaptDlgGraphicFilter = 'Графические файлы|*.bmp;*.png;*.tif;*.jpg;*.jpeg';

  //локализованное наименование кнопок в диалогах
  CaptDlgBtnOK = 'OK';
  CaptDlgBtnYes = 'Да';
  CaptDlgBtnNo = 'Нет';
  CaptDlgBtnCancel = 'Отмена';
  CaptDlgBtnAbort = 'Прервать';
  CaptDlgBtnSave = 'Сохранить';
  CaptDlgBtnAdd = 'Добавить';
  CaptDlgBtnEdit = 'Редактировать';
  CaptDlgBtnDel = 'Удалить';
  CaptDlgBtnAllRight = 'Понятно';
  CaptDlgBtnSettings = 'Настройки';
  CaptDlgBtnSearch = 'Поиск';
  CaptDlgBtnSearchUp = '^';

  CaptDlgTglBoxSettingsShow = 'Показать настройки';
  CaptDlgTglBoxSettingsHide = 'Скрыть настройки';

  //дефолтовые шоткаты для некоторых кнопок
  CaptDefaultShortCutBtnOK = 'Ctrl+Enter';
  CaptDefaultShortCutBtnCancel = 'Esc';

  //"древовидные" константы
  CaptTreeNodeRootRequired = 'Добавьте хотя бы один родительский узел';
  CaptTreeNodeSelectRequired = 'Выделите хотя бы один узел в дереве';
  CaptTreeNodeCheckedRequired = 'Отметьте хотя бы один узел в дереве';
  CaptTreeNodeCannotBeEmpty = 'Название узла не может быть пустым';
  CaptTreeNodeCannotEditMultiSelect = 'Можно отредактировать только один выделенный узел в дереве';
  CaptTreeNodeCannotDeleteMultiSelect = 'Можно удалить только один выделенный узел в дереве';
  CaptTreeNodeChildRequired = 'Добавьте значение хотя бы для одного дочернего узла';
  CaptTreeNodeRootMissing = 'Ранее выбранный корневой узел отсутствует';
  CaptTreeNodeEditDeniedAccess = 'Недостаточно прав на изменение/удаление узла';
  CaptTreeNodeColumnNameCaption = 'Краткое описание';
  CaptTreeNodeColumnNameDescript = 'Подробное описание';

  //"поисковые" константы
   CaptItemSearchNodeFieldCaption = 'Краткое описание';
   CaptItemSearchNodeFieldDescript = 'Полное описание';
  {$ENDREGION}

implementation

end.

