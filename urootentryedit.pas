unit urootentryedit;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ActnList,
  LCLProc, ExtCtrls, uresourcestring, LazUTF8;

type

  { TfrmRootEntryEdit }

  TfrmRootEntryEdit = class(TForm)
    ActBtnCancel: TAction;
    ActBtnOK: TAction;
    ActListRootCaptionEdit: TActionList;
    btnRight: TButton;
    btnLeft: TButton;
    edtRootCaption: TEdit;
    lblRootCaption: TLabel;
    pnlBtn: TPanel;
    procedure ActBtnCancelExecute(Sender: TObject);
    procedure ActBtnOKExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private

  public

  end;

var
  frmRootEntryEdit: TfrmRootEntryEdit;

implementation

{$R *.lfm}

{ TfrmRootEntryEdit }

procedure TfrmRootEntryEdit.FormCreate(Sender: TObject);
var
  txtlen: PtrInt = 0;
  i: PtrInt = 0;
begin
  Self.ModalResult:= mrCancel;
  Self.AutoSize:= True;
  Self.AutoScroll:= True;
  Self.KeyPreview:= True;
  //Self.ShowHint:= True;
  Self.BorderIcons:= [biSystemMenu];
  Self.Position:= poOwnerFormCenter;
  edtRootCaption.Constraints.MinWidth:= Canvas.TextWidth(lblRootCaption.Caption) * 3;
  pnlBtn.Caption:= '';
  pnlBtn.Constraints.MinHeight:= btnRight.Height + btnRight.BorderSpacing.Around * 2;
  pnlBtn.BevelOuter:= bvNone;

  ActBtnOK.ShortCut:= TextToShortCut(CaptDefaultShortCutBtnOK);
  ActBtnCancel.ShortCut:= TextToShortCut(CaptDefaultShortCutBtnCancel);

  {$IFDEF MSWINDOWS}
  btnLeft.Caption:= CaptDlgBtnSave;
  btnLeft.OnClick:= @ActBtnOKExecute;

  btnRight.Caption:= CaptDlgBtnCancel;
  btnRight.OnClick:= @ActBtnCancelExecute;

  if (UTF8LowerCase(ShortCutToText(ActBtnCancel.ShortCut)) <> 'unknown')
    then btnRight.Hint:= Format('<%s>',[ShortCutToText(ActBtnCancel.ShortCut)])
    else btnRight.Hint:= '';
  if (UTF8LowerCase(ShortCutToText(ActBtnOK.ShortCut)) <> 'unknown')
    then btnLeft.Hint:= Format('<%s>',[ShortCutToText(ActBtnOK.ShortCut)])
    else btnLeft.Hint:= '';
  {$ELSE}
  btnLeft.Caption:= CaptDlgBtnCancel;
  btnLeft.OnClick:= @ActBtnCancelExecute;

  btnRight.Caption:= CaptDlgBtnSave;
  btnRight.OnClick:= @ActBtnOKExecute;

  if (UTF8LowerCase(ShortCutToText(ActBtnCancel.ShortCut)) <> 'unknown')
    then btnLeft.Hint:= Format('<%s>',[ShortCutToText(ActBtnCancel.ShortCut)])
    else btnLeft.Hint:= '';
  if (UTF8LowerCase(ShortCutToText(ActBtnOK.ShortCut)) <> 'unknown')
    then btnRight.Hint:= Format('<%s>',[ShortCutToText(ActBtnOK.ShortCut)])
    else btnRight.Hint:= '';
  {$ENDIF}

  for i:= 0 to Pred(pnlBtn.ControlCount) do
    if TObject(pnlBtn.Controls[i]).InheritsFrom(TButton) then
      if (txtlen < pnlBtn.Canvas.TextWidth(TButton(pnlBtn.Controls[i]).Caption)) then
        txtlen:= pnlBtn.Canvas.TextWidth(TButton(pnlBtn.Controls[i]).Caption);

  for i:= 0 to Pred(pnlBtn.ControlCount) do
    if TObject(pnlBtn.Controls[i]).InheritsFrom(TButton) then
    begin
      TButton(pnlBtn.Controls[i]).AutoSize:= False;
      TButton(pnlBtn.Controls[i]).Width:= txtlen + pnlBtn.Canvas.TextWidth('W') * 2;
      TButton(pnlBtn.Controls[i]).ShowHint:= True;
    end;
end;

procedure TfrmRootEntryEdit.FormClose(Sender: TObject;
  var CloseAction: TCloseAction);
begin
  CloseAction:= caFree;
end;

procedure TfrmRootEntryEdit.ActBtnCancelExecute(Sender: TObject);
begin
  Self.ModalResult:= mrCancel;
end;

procedure TfrmRootEntryEdit.ActBtnOKExecute(Sender: TObject);
begin
  if (UTF8Trim(edtRootCaption.Text) = '') then
  begin
    QuestionDlg(MsgCaptNotEnoughData,
                CaptTreeNodeCannotBeEmpty,
                mtInformation,
                [mrOK,CaptDlgBtnAllRight],
                0
                );
    if edtRootCaption.CanSetFocus then edtRootCaption.SetFocus;
    Exit;
  end;
  Self.ModalResult:= mrOK;
end;

end.

