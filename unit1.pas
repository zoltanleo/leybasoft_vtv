unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes
  , SysUtils
  , Math
  , DB
  , Forms
  , Controls
  , Graphics
  , Dialogs
  , StdCtrls
  , ActnList
  , LCLType
  , ComCtrls
  , ExtCtrls
  , uresourcestring
  , ufredit
  , ucustomtypes
  , DateUtils
  , IBDatabase
  , IBQuery
  , IB
  , IBSQL
  , LazFileUtils
  , LazUTF8
  , laz.VirtualTrees
  , Generics.Collections
  ;

type
  TPnlObjList = specialize TObjectList<TfrEdit>;

  { TForm1 }

  TForm1 = class(TForm)
    ActConnectDB: TAction;
    ActFillVST: TAction;
    ActFrameCreate: TAction;
    ActFrameRemove: TAction;
    ActFillcbbRoot: TAction;
    ActEntryDel: TAction;
    ActEntryAddEdit: TAction;
    ActEditNodeSave: TAction;
    ActEditNodeCancel: TAction;
    ActFillcbbSearchingField: TAction;
    ActReport: TAction;
    ActSearchNode: TAction;
    ActRefreshVTV: TAction;
    ActRootAddEdit: TAction;
    ActEditNodeStart: TAction;
    ActEditNodeFinish: TAction;
    ActqryTreeRootOpen: TAction;
    actList: TActionList;
    btnRootAdd: TButton;
    btnRootEdit: TButton;
    btnShow: TButton;
    btnNodeAdd: TButton;
    btnCancelEdit: TButton;
    btnNodeDel: TButton;
    btnNodeEdit: TButton;
    btnSaveEdit: TButton;
    btnDelesect: TButton;
    btnSearch: TButton;
    btnSearchDown: TButton;
    btnSearchUp: TButton;
    cbbRoot: TComboBox;
    cbbRR: TComboBox;
    cbbCh: TComboBox;
    cbbChCh: TComboBox;
    cbbSChSCh: TComboBox;
    chbAutoExpand: TCheckBox;
    chbExpand: TCheckBox;
    chbIncludeCh: TCheckBox;
    chbRootHasCheckbox: TCheckBox;
    chbRootTextInclude: TCheckBox;
    chbAllowShowNonOwnEntries: TCheckBox;
    chbAllowEditNonOwnEntries: TCheckBox;
    chbHeaderShow: TCheckBox;
    chbShowSearchBox: TCheckBox;
    chbSelectAllFoundNodes: TCheckBox;
    chbSearchCaseSensitive: TCheckBox;
    chbIncludeRR: TCheckBox;
    cbbR: TComboBox;
    edtR: TEdit;
    edtRR: TEdit;
    edtCh: TEdit;
    edtChCh: TEdit;
    edtSChSCh: TEdit;
    edtSearchText: TEdit;
    IBDB: TIBDatabase;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Panel1: TPanel;
    qryTreeSubChild: TIBQuery;
    pnlSearch: TPanel;
    pnlSettings: TPanel;
    pnlBtn: TPanel;
    rgCh: TRadioGroup;
    rgSch: TRadioGroup;
    rgSearchField: TRadioGroup;
    splMemo: TSplitter;
    splVST: TSplitter;
    sqlExecTree: TIBSQL;
    Memo1: TMemo;
    pnlFrame: TPanel;
    scrboxFr: TScrollBox;
    tboxSettings: TToggleBox;
    VST: TLazVirtualStringTree;
    qryTreeChild: TIBQuery;
    qryTreeRoot: TIBQuery;
    TransR: TIBTransaction;
    TransW: TIBTransaction;
    procedure ActConnectDBExecute(Sender: TObject);
    procedure ActEditNodeCancelExecute(Sender: TObject);
    procedure ActEditNodeSaveExecute(Sender: TObject);
    procedure ActEntryAddEditExecute(Sender: TObject);
    procedure ActFillcbbRootExecute(Sender: TObject);
    procedure ActFillVSTExecute(Sender: TObject);
    procedure ActFrameCreateExecute(Sender: TObject);
    procedure ActFrameRemoveExecute(Sender: TObject);
    procedure ActqryTreeRootOpenExecute(Sender: TObject);
    procedure ActReportExecute(Sender: TObject);
    procedure ActRootAddEditExecute(Sender: TObject);
    procedure ActEntryDelExecute(Sender: TObject);
    procedure ActEditNodeFinishExecute(Sender: TObject);
    procedure ActEditNodeStartExecute(Sender: TObject);
    procedure ActRefreshVTVExecute(Sender: TObject);
    procedure ActSearchNodeExecute(Sender: TObject);
    procedure btnDelesectClick(Sender: TObject);
    procedure btnShowClick(Sender: TObject);
    procedure chbAllowEditNonOwnEntriesChange(Sender: TObject);
    procedure chbAllowShowNonOwnEntriesChange(Sender: TObject);
    procedure chbAutoExpandChange(Sender: TObject);
    procedure chbExpandChange(Sender: TObject);
    procedure chbHeaderShowChange(Sender: TObject);
    procedure chbIncludeChChange(Sender: TObject);
    procedure chbIncludeRRChange(Sender: TObject);
    procedure chbRootHasCheckboxChange(Sender: TObject);
    procedure chbRootTextIncludeChange(Sender: TObject);
    procedure chbSearchCaseSensitiveChange(Sender: TObject);
    procedure chbSelectAllFoundNodesChange(Sender: TObject);
    procedure chbShowSearchBoxChange(Sender: TObject);
    procedure edtSearchTextChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure tboxSettingsChange(Sender: TObject);
    procedure VSTAddToSelection(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure VSTBeforeCellPaint(Sender: TBaseVirtualTree;
      TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex;
      CellPaintMode: TVTCellPaintMode; CellRect: TRect; var ContentRect: TRect);
    procedure VSTChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure VSTChecked(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure VSTClick(Sender: TObject);
    procedure VSTFocusChanged(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex);
    procedure VSTFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure VSTGetNodeDataSize(Sender: TBaseVirtualTree;
      var NodeDataSize: Integer);
    procedure VSTGetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; TextType: TVSTTextType; var CellText: String);
    procedure VSTIncrementalSearch(Sender: TBaseVirtualTree;
      Node: PVirtualNode; const SearchText: String; var Result: Integer);
    procedure VSTInitNode(Sender: TBaseVirtualTree; ParentNode,
      Node: PVirtualNode; var InitialStates: TVirtualNodeInitStates);
    procedure VSTMeasureItem(Sender: TBaseVirtualTree; TargetCanvas: TCanvas;
      Node: PVirtualNode; var NodeHeight: Integer);
    procedure VSTPaintText(Sender: TBaseVirtualTree;
      const TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex;
      TextType: TVSTTextType);
    procedure VSTRemoveFromSelection(Sender: TBaseVirtualTree;
      Node: PVirtualNode);
    procedure VSTStateChange(Sender: TBaseVirtualTree; Enter,
      Leave: TVirtualTreeStates);
  private
    FCurRole: String;
    FCurUser: String;
    FDepartmentKind: String;
    FLastSelNodeParentID: PtrInt;//Data.ID родителя последнего выделенного в дереве узла
    FParentID: PtrInt;//текущее значение ID "родительского" узла
    FcbbRootTextValue: String;//текущее значение cbbRoot.Text
    FEditMode: TEditMode;//режим редактирования данных
    FIsAllowEditNonMyOwnedEntries: Boolean;//разрешение на редактирование/удаление "не своих" записей
    FIsAllowShowNonMyOwnedEntries: Boolean;//разрешение на показ "не своих" записей
    FIsAllRootNodesExpand: Boolean;//раскрытие/скрытие всех root-узлов при отображении
    FIsCaseSensitiveSearch: Boolean;//поиск с учетом регистра
    FIsRootHasCheckBox: Boolean;//наличие чекбокса у root
    FIsRootTextInclude: Boolean;//включение заголовка root в итоговый текст
    FIsSelectAllFoundNodes: Boolean;//выделять все найденные узлы
    FIsShowColumnHeader: Boolean;//показывать заголовки колонок
    FIsShowPnlSearch: Boolean;//показ панели поиска
    FLastSelectedNodeID: PtrInt;//Data.ID последнего выделенного в дереве узла
    FPnlObjList: TPnlObjList;//список с фреймами
    FIsQryTreeRootUpdated: Boolean;//флаг изменения qryTreeRoot
    FIsQryTreeChildUpdated: Boolean;//флаг изменения qryTreeChild
    FTblName: String;
    FLastFoundNode: PVirtualNode;//последний найденный узел при поиске
    FSomeCheckBoxHeight: LongInt;
    procedure SetDepartmentKind(AValue: String);
    procedure SetPnlObjList(AValue: TPnlObjList);
  public
    property PnlObjList: TPnlObjList read FPnlObjList write SetPnlObjList;//список объектов-фреймов
    property EditMode: TEditMode read FEditMode write FEditMode;//режим редактирования окна
    property CurUser: String read FCurUser;//имя текущего юзера
    property CurRole: String read FCurRole;//роль текущего юзера
    property IsRootHasCheckBox: Boolean read FIsRootHasCheckBox write FIsRootHasCheckBox;//наличие чекбокса у root
    property IsRootTextInclude: Boolean read FIsRootTextInclude write FIsRootTextInclude;//включение заголовка root в итоговый текст
    property IsAllRootNodesExpand: Boolean read FIsAllRootNodesExpand write FIsAllRootNodesExpand;//раскрытие/скрытие всех root-узлов при отображении
    property LastSelectedNodeID: PtrInt read FLastSelectedNodeID;//Data.ID последнего выделенного в дереве узла
    property LastSelNodeParentID: PtrInt read FLastSelNodeParentID;//Data.ID родителя последнего выделенного в дереве узла
    property LastFoundNode: PVirtualNode read FLastFoundNode;//последний найденный узел при поиске
    property IsAllowEditNonMyOwnedEntries: Boolean read FIsAllowEditNonMyOwnedEntries
      write FIsAllowEditNonMyOwnedEntries;//разрешение на редактирование/удаление "не своих" записей
    property IsAllowShowNonMyOwnedEntries: Boolean read FIsAllowShowNonMyOwnedEntries
          write FIsAllowShowNonMyOwnedEntries;//разрешение на показ "не своих" записей
    property IsShowColumnHeader: Boolean read FIsShowColumnHeader write FIsShowColumnHeader;//показывать заголовки колонок
    property IsShowPnlSearch: Boolean read FIsShowPnlSearch write FIsShowPnlSearch;//показ панели поиска
    property IsSelectAllFoundNodes: Boolean read FIsSelectAllFoundNodes write FIsSelectAllFoundNodes;//выделять все найденные узлы
    property IsCaseSensitiveSearch: Boolean read FIsCaseSensitiveSearch write FIsCaseSensitiveSearch;//поиск с учетом регистра
    property DepartmentKind: String read FDepartmentKind write SetDepartmentKind;//выбираемый раздел
    property TblName: String read FTblName write FTblName;//имя таблицы, передаваемой в запрос
    function GetNodeByID(Sender: TBaseVirtualTree;const aID: PtrUInt): PVirtualNode;
  end;

const
  DBMainName = 'BASE_VTV_0_0_32.FDB';
  UsrName = 'sysdba';
  pwd = 'cooladmin';

  {$IFDEF MSWINDOWS}
    ConnectStr = '127.0.0.1/31064:d:\Archive\my_repo\leybasoft_vtv\dbase\' + DBMainName;
    LibName = 'D:\Portable_program\Firebird_server\Firebird_3_0_10_x64\fbclient.dll';
  {$ELSE}
    {$IFNDEF DARWIN}
      ConnectStr = '127.0.0.1:/home/leyba/laz_proj/leybasoft_vtv/dbase/' + DBMainName;
      LibName = '/opt/firebird/lib/libfbclient.so.3.0.10';
    {$ELSE}
      ConnectStr = '127.0.0.1:/Users/admin/laz_project/leybasoft_vtv/dbase/' + DBMainName;
      LibName = '/Library/Frameworks/Firebird.framework/Versions/A/Resources/lib/libfbclient.dylib';
    {$ENDIF}
  {$ENDIF}

const
  MaxPnlCount = 3;

  SQLRootSelect =
      'SELECT ID, PARENT_ID, CAPTION, DESCRIPTION, OWNER_USER, ' +
        'OWNER_ROLE ' +
      'FROM %s ' +
      'WHERE ((PARENT_ID = :prmParentID) AND (DELETED_REC = 0) AND (DEPART_KIND = :prmDepartKind) ';

  SQLRootInsert =
      'INSERT INTO %s (CAPTION, DEPART_KIND) ' +
      'VALUES (:prmCaption, :prmDepartKind) ' +
      'RETURNING ID';

  SQLRootUpdate =
      'UPDATE %s ' +
      'SET CAPTION = :prmCaption ' +
      'WHERE (ID = :prmID)';

  SQLChildSelect =
      'SELECT ID, PARENT_ID, CAPTION, DESCRIPTION, OWNER_USER, OWNER_ROLE ' +
      'FROM %s ' +
      'WHERE ((DELETED_REC = 0) AND (PARENT_ID = :prmParentID) AND (DEPART_KIND = :prmDepartKind) ';

  SQLChildInsert =
      'INSERT INTO %s ' +
        '(PARENT_ID, CAPTION, DESCRIPTION, DEPART_KIND) ' +
      'VALUES ' +
        '(:prmParentID, :prmCaption, :prmDescription, :prmDepartKind) ' +
      'RETURNING ID';

  SQLChildUpdate =
      'UPDATE %s ' +
      'SET PARENT_ID = :prmParentID, ' +
          'CAPTION = :prmCaption, ' +
          'DESCRIPTION = :prmDescription ' +
      'WHERE (ID = :prmID)';

  SQLChildDelete =
      'UPDATE %s ' +
      'SET DELETED_REC = 1 ' +
      'WHERE (ID = :prmID)';


  cbbTextItemEmpty = 'ничего';
  cbbTextItemSpace = 'пробел';
  cbbTextItemLineBreak = 'перенос строки';
var
  Form1: TForm1;

implementation

uses urootentryedit;

{$R *.lfm}

//type
//  PTreeData = ^TTreeData;
//  TTreeData = record
//    StrCaption: String;//короткое наименование
//    StrDescript: String;//подробное наименование
//    ID: PtrInt;
//    ParentID: PtrInt;
//    OwnerUser: String;//владелец-юзер
//    OwnerRole: String;//владелец-роль
//  end;

{ TForm1 }

procedure TForm1.FormShow(Sender: TObject);
var
  txtlen: PtrInt = 0;
  i: PtrInt = 0;
begin

  //VST.Header.Columns.Items[0].Width:= 150;
  FDepartmentKind:= 'COMPLAINT';
  FTblName:= 'TBL_COMPLAINTS';

  chbRootHasCheckboxChange(Sender);//проверяем чекбоксы у root
  chbRootTextIncludeChange(Sender);
  chbAutoExpandChange(Sender);

  Memo1.Visible:= True;
  Memo1.Anchors:= Memo1.Anchors + [akLeft];
  pnlFrame.Visible:= not Memo1.Visible;
  pnlFrame.Anchors:= pnlFrame.Anchors + [akRight];

  tboxSettingsChange(Sender);
  chbAllowShowNonOwnEntriesChange(Sender);
  chbShowSearchBoxChange(Sender);

  ActConnectDBExecute(Sender);

  FParentID:= -1;//
  ActqryTreeRootOpenExecute(Sender);

  if not qryTreeRoot.Active then Exit;

  ActFillVSTExecute(Sender);
  chbAutoExpandChange(Sender);
  chbExpandChange(Sender);
  chbHeaderShowChange(Sender);
  VST.Selected[VST.GetFirst(True)]:= True;//выделяем первый узел

  with btnShow do
  begin
    Enabled:= True;
    Caption:= 'Показать';
    Tag:= -1;
  end;

  with btnNodeAdd do begin
    Enabled:= True;
    Caption:= CaptDlgBtnAdd;
    Tag:= -1;
  end;

  with btnNodeEdit do
  begin
    Enabled:= (not qryTreeRoot.IsEmpty) and Assigned(VST.GetFirst(True));
    Caption:= CaptDlgBtnEdit;
    Tag:= -1;
  end;

  with btnNodeDel do
  begin
    Enabled:= not qryTreeRoot.IsEmpty and Assigned(VST.GetFirst(True));
    Caption:= CaptDlgBtnDel;
    Tag:= -1;
  end;

  with btnSaveEdit do
  begin
    Visible:= False;
    Caption:= CaptDlgBtnSave;
    Tag:= -2;
  end;

  with btnCancelEdit do
  begin
    Visible:= False;
    Caption:= CaptDlgBtnCancel;
    Tag:= -2;
  end;

  for i:= 0 to Pred(pnlBtn.ControlCount) do
    if TObject(pnlBtn.Controls[i]).InheritsFrom(TButton) then
      if (txtlen < pnlBtn.Canvas.TextWidth(TButton(pnlBtn.Controls[i]).Caption)) then
        txtlen:= pnlBtn.Canvas.TextWidth(TButton(pnlBtn.Controls[i]).Caption);

  for i:= 0 to Pred(pnlBtn.ControlCount) do
    if TObject(pnlBtn.Controls[i]).InheritsFrom(TButton) then
    begin
      {$IFDEF MSWINDOWS}
      TButton(pnlBtn.Controls[i]).AutoSize:= True;
      {$ELSE}
        {$IFDEF LINUX}
        TButton(pnlBtn.Controls[i]).AutoSize:= False;
        TButton(pnlBtn.Controls[i]).Height:= pnlBtn.Canvas.TextHeight('W') * 3 div 2;
        {$ENDIF}
      {$ENDIF}

      TButton(pnlBtn.Controls[i]).Constraints.MinWidth:= txtlen + pnlBtn.Canvas.TextWidth('W') * 2;
      TButton(pnlBtn.Controls[i]).ShowHint:= True;

    end;

  tboxSettings.Height:= btnNodeAdd.Height;

  with btnSearch do
  begin
    Caption:= CaptDlgBtnSearch;
    Width:= txtlen + Canvas.TextWidth('W') * 2;;
  end;

  chbIncludeRR.Checked:= True;
  chbIncludeCh.Checked:= True;
  chbIncludeRRChange(Sender);
  chbIncludeChChange(Sender);
end;

procedure TForm1.tboxSettingsChange(Sender: TObject);
begin
  pnlSettings.Visible:= tboxSettings.Checked;

  if tboxSettings.Checked
    then tboxSettings.Caption:= CaptDlgTglBoxSettingsHide
    else tboxSettings.Caption:= CaptDlgTglBoxSettingsShow;

  tboxSettings.Width:= Self.Canvas.TextWidth(tboxSettings.Caption) + Self.Canvas.TextWidth('W') * 2;
end;

procedure TForm1.VSTAddToSelection(Sender: TBaseVirtualTree; Node: PVirtualNode
  );
begin
  FLastSelectedNodeID:= PTreeData(TBaseVirtualTree(Sender).GetNodeData(Node))^.ID;

  if (TBaseVirtualTree(Sender).GetNodeLevel(Node) = 0)//это root
    then FLastSelNodeParentID:= -1
    else FLastSelNodeParentID:= PTreeData(TBaseVirtualTree(Sender).GetNodeData(Node^.Parent))^.ID;
end;

procedure TForm1.VSTBeforeCellPaint(Sender: TBaseVirtualTree;
  TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex;
  CellPaintMode: TVTCellPaintMode; CellRect: TRect; var ContentRect: TRect);
var
  aNodeData: PTreeData = nil;
begin
  aNodeData:= TBaseVirtualTree(Sender).GetNodeData(Node);

  //рисуем фон по условию
  if (IsAllowShowNonMyOwnedEntries and (aNodeData^.OwnerUser <> CurUser)) then
    begin
      TargetCanvas.Brush.Color := clBtnShadow;
      TargetCanvas.FillRect(CellRect);
    end;
end;

procedure TForm1.VSTChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
begin
  VST.Refresh;
  Caption:= Format('Parent ID: %d | Child ID: %d',
            [LastSelNodeParentID, LastSelectedNodeID]);
  //Caption:= IntToStr(LastSelectedNodeID);
  //Caption:= IntToStr(LastSelNodeParentID);
end;

procedure TForm1.VSTChecked(Sender: TBaseVirtualTree; Node: PVirtualNode);
var
  RootNode: PVirtualNode = nil;
  ChildNode: PVirtualNode = nil;
  SubChildNode: PVirtualNode = nil;
  aNodeLvl: PtrInt = -1;
  aChkCount: PtrInt = 0;//кол-во "чекнутых" деток

  //получаем CheckState Parent-узла по его деткам
  function GetParentNodeStateByChilds(Sender: PVirtualNode): TCheckState;
  var
    aNode: PVirtualNode = nil;
    ChildNode: PVirtualNode = nil;
    ChkCount: PtrInt = 0;//кол-во "чекнутых" деток
  begin
    Result:= csUncheckedNormal;
    aNode:= PVirtualNode(Sender);

    if (aNode^.ChildCount = 0) then Exit;

    ChildNode:= aNode^.FirstChild;
    ChkCount:= 0;

    while Assigned(ChildNode) do
    begin
      if (ChildNode^.CheckState = csCheckedNormal)
        then Inc(ChkCount)
        else
          if (ChildNode^.CheckState = csMixedNormal) then
          begin
            ChkCount:= -1;
            Break;
          end;
      ChildNode:= ChildNode^.NextSibling;//следующий дочерний
    end;

    if (ChkCount = aNode^.ChildCount)
      then Result:= csCheckedNormal //все дочки отмечены
      else
        if (ChkCount = 0)
          then Result:= csUncheckedNormal //ничего не отмечено
          else Result:= csMixedNormal; //часть дочек отмечена
  end;

begin
  aNodeLvl:= TBaseVirtualTree(Sender).GetNodeLevel(Node);
  case aNodeLvl of
    0:
      begin
        if (Node^.CheckType <> ctCheckBox) then Exit;
        if (Node^.ChildCount > 0) then
        begin
          ChildNode:= Node^.FirstChild;//первый дочерний
          while Assigned(ChildNode) do //пока не кончились дочерние узлы
          begin
            ChildNode^.CheckState:= Node^.CheckState;

            if (ChildNode^.ChildCount > 0 ) then
            begin
              SubChildNode:= ChildNode^.FirstChild;
              while Assigned(SubChildNode) do
              begin
                SubChildNode^.CheckState:= Node^.CheckState;
                SubChildNode:= SubChildNode^.NextSibling;
              end;
            end;
            ChildNode:= ChildNode^.NextSibling;//следующий дочерний
          end;
        end;
      end;
    1:
      begin
        if (PVirtualNode(Node^.Parent)^.CheckType = ctCheckBox)
          then PVirtualNode(Node^.Parent)^.CheckState:= GetParentNodeStateByChilds(Node^.Parent);

        if (Node^.ChildCount > 0) then
        begin
          ChildNode:= Node^.FirstChild;//первый дочерний
          while Assigned(ChildNode) do //пока не кончились дочерние узлы
          begin
            ChildNode^.CheckState:= Node^.CheckState;
            ChildNode:= ChildNode^.NextSibling;//следующий дочерний
          end;
        end;
      end;
    2:
      begin
        {for Nodelevel = 1}
        if (PVirtualNode(Node^.Parent)^.CheckType = ctCheckBox) then
          PVirtualNode(Node^.Parent)^.CheckState:= GetParentNodeStateByChilds(Node^.Parent);

       {for Nodelevel = 0}
       RootNode:= PVirtualNode(Node^.Parent)^.Parent;
        if (RootNode^.CheckType = ctCheckBox) then
          RootNode^.CheckState:= GetParentNodeStateByChilds(RootNode);
      end;
  end;
  VST.Refresh;
end;

procedure TForm1.VSTClick(Sender: TObject);
begin
  if (TBaseVirtualTree(Sender).SelectedCount = 0) then FLastSelectedNodeID:= -1;
end;

procedure TForm1.VSTFocusChanged(Sender: TBaseVirtualTree; Node: PVirtualNode;
  Column: TColumnIndex);
begin
  VST.Refresh;
end;

procedure TForm1.VSTFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
var
  Data: PTreeData;
begin
  Data := VST.GetNodeData(Node);
  if Assigned(Data) then
  begin
    Data^.StrCaption := '';
    Data^.StrDescript := '';
    Data^.OwnerUser := '';
    Data^.OwnerRole := '';
  end;
end;

procedure TForm1.VSTGetNodeDataSize(Sender: TBaseVirtualTree;
  var NodeDataSize: Integer);
begin
  NodeDataSize:= SizeOf(TTreeData);
end;

procedure TForm1.VSTGetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
  Column: TColumnIndex; TextType: TVSTTextType; var CellText: String);
var
  Data: PTreeData;
begin
  Data := VST.GetNodeData(Node);
  case Column of
    0: CellText := Data^.StrCaption;
    1: CellText := Data^.StrDescript;
  end;
end;

procedure TForm1.VSTIncrementalSearch(Sender: TBaseVirtualTree;
  Node: PVirtualNode; const SearchText: String; var Result: Integer);
var
  aDataNode: PTreeData = nil;
  //Len: PtrInt = 0;
begin
  Result := 0;
  aDataNode := VST.GetNodeData(Node);
  if Assigned(aDataNode) then
  begin
    // Используя StrLIComp, мы можем указать длину сравнения.
    // Таким образом, мы сможем найти узлы, совпадающие частично.
    Result := StrLIComp(PUTF8Char(SearchText),
                        PUTF8Char(aDataNode^.StrCaption),
                        Math.Min(UTF8Length(SearchText), UTF8Length(aDataNode^.StrCaption)));
  end;
end;

procedure TForm1.VSTInitNode(Sender: TBaseVirtualTree; ParentNode,
  Node: PVirtualNode; var InitialStates: TVirtualNodeInitStates);
var
  aNodeLvl: PtrInt = -1;
begin
  aNodeLvl := VST.GetNodeLevel(Node);//уровень вложенности
  case aNodeLvl of
    0:
      begin
        if IsRootHasCheckBox
          then Node^.CheckType:= ctCheckBox//Root (пустой checkbox)
          else Node^.CheckType:= ctNone;
      end;
  1,2:
      begin
        Node^.CheckType:= ctCheckBox;
      end;
  end;

end;

procedure TForm1.VSTMeasureItem(Sender: TBaseVirtualTree;
  TargetCanvas: TCanvas; Node: PVirtualNode; var NodeHeight: Integer);
begin
  if (Node^.CheckType = ctCheckBox)
    then NodeHeight:= FSomeCheckBoxHeight;
  TBaseVirtualTree(Sender).RepaintNode(Node);
end;

procedure TForm1.VSTPaintText(Sender: TBaseVirtualTree;
  const TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex;
  TextType: TVSTTextType);
var
  aNodeData: PTreeData = nil;
begin
  aNodeData:= TBaseVirtualTree(Sender).GetNodeData(Node);

  //рисует шрифт по условию
  if (IsAllowShowNonMyOwnedEntries and (aNodeData^.OwnerUser <> CurUser)) then
    begin
      TargetCanvas.Font.Color:= clMaroon;
      if (vsSelected in Node^.States) and (Sender.Focused) then
      begin
        TargetCanvas.Font.Color := clBtnHighlight;
        TargetCanvas.Font.Style:= [fsItalic];
      end;
    end;

end;

procedure TForm1.VSTRemoveFromSelection(Sender: TBaseVirtualTree;
  Node: PVirtualNode);
begin
  //if (TBaseVirtualTree(Sender).SelectedCount = 0) then FLastSelectedNodeID:= -1;
end;

procedure TForm1.VSTStateChange(Sender: TBaseVirtualTree; Enter,
  Leave: TVirtualTreeStates);
begin
  //if tsChangePending in Leave then DoSomething;
end;

procedure TForm1.SetPnlObjList(AValue: TPnlObjList);
begin
  if FPnlObjList=AValue then Exit;
  FPnlObjList:=AValue;
end;

function TForm1.GetNodeByID(Sender: TBaseVirtualTree; const aID: PtrUInt
  ): PVirtualNode;
var
  Node: PVirtualNode = nil;
  NodeData: PTreeData = nil;
  function FindNestedNode(aNode: PVirtualNode):  PVirtualNode;
  //var
  //  aNodeData: PTreeData = nil;
  begin
    Result:= nil;

    while Assigned(aNode) do
    begin
      NodeData:= TBaseVirtualTree(Sender).GetNodeData(aNode);

      if (NodeData^.ID = aID)
      then
        begin
          Result:= aNode;
          Exit;
        end
      else
        if (aNode^.ChildCount > 0) then
          begin
            Result:= FindNestedNode(aNode^.FirstChild);
            if Assigned(Result) then Exit;
          end;

      aNode:= aNode^.NextSibling;
    end;
  end;

begin
  Result:= nil;
  if TBaseVirtualTree(Sender).IsEmpty then Exit;

  Node:= TBaseVirtualTree(Sender).GetFirst(True);

  while Assigned(Node) do
  begin
    NodeData:= TBaseVirtualTree(Sender).GetNodeData(Node);
    if (aID = NodeData^.ID)
    then
      begin
        Result:= Node;
        Exit;
      end
    else
      if (Node^.ChildCount > 0) then
      begin
        Result:= FindNestedNode(Node^.FirstChild);
        if Assigned(Result) then exit;
      end;

    Node:= Node^.NextSibling;
  end;
end;

procedure TForm1.SetDepartmentKind(AValue: String);
begin
  if FDepartmentKind=AValue then Exit;
  FDepartmentKind:=AValue;
end;

procedure TForm1.ActConnectDBExecute(Sender: TObject);
var
  ErrMsg: String = '';
  TmpQry: TIBQuery = nil;
begin
  with IBDB do
  begin
    try
      if Connected then Exit;
      DatabaseName:= ConnectStr;
      LibraryName:= LibName;
      Params.Add('lc_ctype=UTF8');
      Params.Add('user_name=' + UsrName);
      Params.Add('password=' + pwd);
      Connected:= True;

      TmpQry:= TIBQuery.Create(Self);
      try
        try
          TmpQry.Database:= IBDB;
          TmpQry.Transaction:= TransR;
          TmpQry.SQL.Text:=
            'SELECT ' +
              'CURRENT_USER AS CUR_USER, ' +
              'CURRENT_ROLE AS CUR_ROLE ' +
            'FROM RDB$DATABASE';
          TmpQry.Active:= True;

          FCurUser:= TmpQry.FN('CUR_USER').AsString;
          FCurRole:= TmpQry.FN('CUR_ROLE').AsString;

        except
          on E:EIBError do
          begin
            {$IFDEF MSWINDOWS}
            ErrMsg:= WinCPToUTF8(E.Message);
            {$ELSE}
            ErrMsg:= E.Message;
            {$ENDIF}

            TmpQry.Active:= False;

            QuestionDlg(MsgCaptErrorReadData,
                        Format(MsgTextControlErrorDetail,
                              [ErrMsg,
                              sLineBreak,
                              Self.UnitName,
                              sLineBreak,
                              TmpQry.Name]),
                        mtError,
                        [mrOK,CaptDlgBtnOK],
                        0
                        );
            Exit;
          end;{on E:EIBError do}
        end;
      finally
        FreeAndNil(TmpQry);
      end;
    except
      on E:EIBError do
      begin
        {$IFDEF MSWINDOWS}
        ErrMsg:= WinCPToUTF8(E.Message);
        {$ELSE}
        ErrMsg:= E.Message;
        {$ENDIF}

        Connected:= False;
        QuestionDlg(MsgCaptErrorConnectDB,
                    Format(MsgTextControlErrorDetail,
                          [ErrMsg,
                          sLineBreak,
                          Self.UnitName,
                          sLineBreak,
                          Name]),
                    mtError,
                    [mrOK,CaptDlgBtnOK],
                    0
                    );
        Exit;
      end;{on E:EIBError do}
    end;{try..except}
  end;{with IBDB do}
end;

procedure TForm1.ActEditNodeCancelExecute(Sender: TObject);
begin
  ActEditNodeFinishExecute(Sender);
  PnlObjList.Clear;

  if FIsQryTreeRootUpdated then
  begin
    FIsQryTreeRootUpdated:= False;
    FParentID:= -1;
    ActqryTreeRootOpenExecute(Sender);
    ActFillVSTExecute(Sender);
    chbExpandChange(Sender);//разворачиваем/сворачиваем узлы
    ActRefreshVTVExecute(Sender);
  end;
end;

procedure TForm1.ActEditNodeSaveExecute(Sender: TObject);
var
  ErrMsg: String = '';
  i: PtrInt = 0;
  IsFrameEmpty: Boolean = True;//заполнен ли значениями один фрейм
  ParentID: PtrInt = -1;
  OldDataNodeID: PtrInt = -1;//значение LastSelectedNodeID до начала манипуляций
begin
  { #todo : задействовать механизм эвентов }
  ActqryTreeRootOpenExecute(Sender);

  if qryTreeRoot.IsEmpty then
  begin
    QuestionDlg(MsgCaptNotEnoughData,
                CaptTreeNodeRootRequired,
                mtInformation,
                [mrOK,CaptDlgBtnAllRight],
                0
                );
    if btnRootAdd.CanSetFocus then btnRootAdd.SetFocus;
    Exit;
  end;

  OldDataNodeID:= LastSelectedNodeID;//сохраняем значение последнего выделенного узла

  if qryTreeRoot.Locate('CAPTION', cbbRoot.Items[cbbRoot.ItemIndex],[])
    then
      ParentID:= qryTreeRoot.FN('ID').AsInteger
    else
      begin
        QuestionDlg(MsgCaptInvalidData,
                    CaptTreeNodeRootMissing,
                    mtWarning,
                    [mrOK,CaptDlgBtnAllRight],
                    0
                    );
        if cbbRoot.CanSetFocus then cbbRoot.SetFocus;
        Exit;
      end;

  for i:= 0 to Pred(PnlObjList.Count) do
    if (UTF8Trim(PnlObjList.Items[i].edtEdit.Text) <> '') then
    begin
      IsFrameEmpty:= False;
      Break;
    end;

  if IsFrameEmpty then
  begin
    QuestionDlg(MsgCaptNotEnoughData,
                CaptTreeNodeChildRequired,
                mtInformation,
                [mrOK,CaptDlgBtnAllRight],
                0
                );
    scrboxFr.ScrollInView(PnlObjList.First);
    if PnlObjList.First.edtEdit.CanSetFocus then PnlObjList.First.edtEdit.SetFocus;
    Exit;
  end;

  with sqlExecTree do
  begin
    try
      TransW.StartTransaction;
      if Open then Close;

      case EditMode of
        emAdd:
          begin
            SQL.Text:= Format(SQLChildInsert,[TblName])
              //'INSERT INTO TBL_COMPLAINTS ' +
              //  '(PARENT_ID, CAPTION, DESCRIPTION, DEPART_KIND) ' +
              //'VALUES ' +
              //  '(:prmParentID, :prmCaption, :prmDescription, :prmDepartKind) ' +
              //'RETURNING ID'
              ;

            Prepare;
            for i:= 0 to Pred(PnlObjList.Count) do
            begin
              if (UTF8Trim(PnlObjList.Items[i].edtEdit.Text) = '') then Continue;
              ParamByName('prmParentID').AsInteger:= ParentID;
              ParamByName('prmDepartKind').AsString:= UTF8UpperCase(DepartmentKind);
              ParamByName('prmCaption').AsString:= UTF8Trim(PnlObjList.Items[i].edtEdit.Text);
              ParamByName('prmDescription').AsString:= UTF8Trim(PnlObjList.Items[i].memoEdit.Text);
              ExecQuery;
              FLastSelectedNodeID:= FieldByName('ID').AsInteger;
            end;
          end;
        emEdit:
          begin
            SQL.Text:= Format(SQLChildUpdate,[TblName])
              //'UPDATE TBL_COMPLAINTS ' +
              //'SET PARENT_ID = :prmParentID, ' +
              //    'CAPTION = :prmCaption, ' +
              //    'DESCRIPTION = :prmDescription ' +
              //'WHERE (ID = :prmID)'
              ;
            Prepare;
            ParamByName('prmID').AsInteger:= PnlObjList.First.ChildID;
            ParamByName('prmParentID').AsInteger:= ParentID;
            ParamByName('prmCaption').AsString:= UTF8Trim(PnlObjList.First.edtEdit.Text);
            ParamByName('prmDescription').AsString:= UTF8Trim(PnlObjList.First.memoEdit.Text);
            ExecQuery;
            //FLastSelectedNodeID:= PnlObjList.First.ChildID;
          end;
      end;

      TransW.Commit;
      PnlObjList.Clear;
      ActEditNodeFinishExecute(Sender);//скрываем панельку редактирования

      //перезаполняем дерево, начиная с Root-узлов
      FParentID:= -1;
      ActqryTreeRootOpenExecute(Sender);
      ActFillVSTExecute(Sender);

      chbExpandChange(Sender);//разворачиваем/сворачиваем узлы
      ActRefreshVTVExecute(Sender);//позицинируем выделение на вставленный/измененный узел
    except
      on E:EIBError do
      begin
        TransW.Rollback;
        FLastSelectedNodeID:= OldDataNodeID;//возвращаем "старое" значение

        {$IFDEF MSWINDOWS}
          ErrMsg:= WinCPToUTF8(E.Message);
        {$ELSE}
          ErrMsg:= E.Message;
        {$ENDIF}

        QuestionDlg(MsgCaptErrorFrameCreating,
                    Format(MsgTextControlErrorDetail,
                          [ErrMsg,
                          sLineBreak,
                          Self.UnitName,
                          sLineBreak,
                          Name]),
                    mtError,
                    [mrOK,CaptDlgBtnOK],
                    0
                    );

        Exit;
      end;{on E:EIBError do}
    end;{try..except}
  end;{with sqlExecTree do}
end;

procedure TForm1.ActEntryAddEditExecute(Sender: TObject);
var
  aNode: PVirtualNode = nil;
  aNodeLvl: PtrInt = -1;
  aNodeData: PTreeData = nil;
  aNodeID: PtrInt = -1;//ID выделенного узла
begin
  if not TObject(Sender).InheritsFrom(TButton) then Exit;
  if not (TButton(Sender).Equals(btnNodeAdd)
      or TButton(Sender).Equals(btnNodeEdit)) then Exit;

  if TButton(Sender).Equals(btnNodeAdd)
  then
    begin
      //дефолтовые значения
      aNodeLvl:= 0;
      FParentID:= -1;
      FcbbRootTextValue:= '';

      EditMode:= emAdd;

      if not (VST.IsEmpty) then
      begin
        aNode:= VST.GetFirstSelected(True);
        if Assigned(aNode) then
        begin
          aNodeLvl:= VST.GetNodeLevel(aNode);
          aNodeData:= VST.GetNodeData(aNode);

          //в cbbRoot будут записи одного с выделенным узлом уровня
          if (aNodeLvl > 0) then FParentID:= aNodeData^.ParentID;

          //строка для выставления cbbRoot.ItemIndex;
          FcbbRootTextValue:= aNodeData^.StrCaption;
        end;
      end;

      //не добавляем узлы больше 2-го уровня
      if (aNodeLvl > 1) then Exit;
    end
  else {TButton(Sender).Equals(btnNodeEdit)}
    begin
      EditMode:= emEdit;

      //не редактируем пустой список
      if VST.IsEmpty then Exit;

      //не редактируем множественное выделение
      if (VST.SelectedCount > 1) then
      begin
        QuestionDlg(MsgCaptWarningData,
                    CaptTreeNodeCannotEditMultiSelect,
                    mtWarning,
                    [mrOK,CaptDlgBtnAllRight],
                    0
                    );
        Exit;
      end;

      aNode:= VST.GetFirstSelected(True);

      if not Assigned(aNode)
      then //не редактируем невыделенный узел
        begin
          QuestionDlg(MsgCaptWarningData,
                      CaptTreeNodeSelectRequired,
                      mtWarning,
                      [mrOK,CaptDlgBtnAllRight],
                      0
                      );
          Exit;
        end
      else
        begin
          aNodeLvl:= VST.GetNodeLevel(aNode);
          aNodeData:= VST.GetNodeData(aNode);

          //запрещаем редактировать "чужие" узлы
          if (not IsAllowEditNonMyOwnedEntries and (aNodeData^.OwnerUser <> CurUser)) then
          begin
            QuestionDlg(MsgCaptErrorAccessDenied,
                        CaptTreeNodeEditDeniedAccess,
                        mtWarning,
                        [mrOK,CaptDlgBtnAllRight],
                        0
                        );
            Exit;
          end;

          if (aNodeLvl = 0)
          then //в cbbRoot будут Root-узлы
            begin
              aNodeID:= aNodeData^.ID;
              FParentID:= aNodeData^.ParentID;//будет -1
              FcbbRootTextValue:= aNodeData^.StrCaption;
            end
          else //в cbbRoot будут узлы того же уровня, что и родитель текущего
            begin
              FParentID:= PTreeData(VST.GetNodeData(PVirtualNode(aNode^.Parent)))^.ParentID;
              FcbbRootTextValue:= PTreeData(VST.GetNodeData(PVirtualNode(aNode^.Parent)))^.StrCaption;;
            end;
        end;{if Assigned(aNode)}
    end;{TButton(Sender).Equals(btnNodeEdit)}

  //прячем/показываем кнопки редактирования Root-узла
  btnRootAdd.Visible:= (aNodeLvl = 0);
  btnRootEdit.Visible:= (aNodeLvl = 0);


  ActqryTreeRootOpenExecute(Sender);//заполняем родительский датасет
  if not qryTreeRoot.Active then Exit;

  ActFillcbbRootExecute(Sender);//заполняем список родительскими узлами

  if qryTreeRoot.IsEmpty
    then cbbRoot.ItemIndex:= 0
    else
      if (cbbRoot.Items.IndexOf(FcbbRootTextValue) <> -1)
        then cbbRoot.ItemIndex:= cbbRoot.Items.IndexOf(FcbbRootTextValue)
        else cbbRoot.ItemIndex:= -1;

  ActEditNodeStartExecute(Sender);//скрываем кнопки
  ActFrameCreateExecute(Sender);//добавляем 1-й фрейм

  if Assigned(PnlObjList) then
  case aNodeLvl of
    0:
      begin
        PnlObjList.First.edtEdit.Clear;
        PnlObjList.First.memoEdit.Clear;

        //вызываем панель редактирования Root-узла
        if TButton(Sender).Equals(btnNodeEdit) then
        begin
          ActRootAddEditExecute(btnRootEdit);
          ActEditNodeCancelExecute(Sender);
        end;
      end;
  1,2:
      begin
        if TButton(Sender).Equals(btnNodeEdit) then
        begin
          PnlObjList.First.edtEdit.Text:= aNodeData^.StrCaption;
          PnlObjList.First.memoEdit.Text:= aNodeData^.StrDescript;
          PnlObjList.First.ChildID:= aNodeData^.ID;
        end;
      end;
  end;
end;

procedure TForm1.ActFillcbbRootExecute(Sender: TObject);
begin
  cbbRoot.Clear;
  cbbRoot.Enabled:= True;

  if qryTreeRoot.IsEmpty
  then
    begin
      cbbRoot.Items.Add(CaptTreeNodeRootRequired);
      cbbRoot.ItemIndex:= 0;
      cbbRoot.Enabled:= False;
    end
  else
    begin
      qryTreeRoot.First;
      while not qryTreeRoot.EOF do
      begin
        cbbRoot.Items.Add(qryTreeRoot.FN('CAPTION').AsString);
        qryTreeRoot.Next;
      end;
    end;
end;

procedure TForm1.ActFillVSTExecute(Sender: TObject);
var
  aNode: PVirtualNode = nil;
  aChildNode: PVirtualNode = nil;
  aSubChildNode: PVirtualNode = nil;
  aNodeData: PTreeData = nil;
  ErrMsg: String = '';
begin
  VST.BeginUpdate;
  try
    try
      VST.Clear;
      if qryTreeRoot.IsEmpty then Exit;

      with qryTreeChild do
      begin
        if Active then Active:= False;
        SQL.Text:= Format(SQLChildSelect,[TblName])
          //'SELECT ID, PARENT_ID, CAPTION, DESCRIPTION, OWNER_USER, OWNER_ROLE ' +
          //'FROM TBL_COMPLAINTS ' +
          //'WHERE ((DELETED_REC = 0) AND (PARENT_ID = :prmParentID) AND (DEPART_KIND = :prmDepartKind) '
          ;

        if IsAllowShowNonMyOwnedEntries
          then SQL.Text:= SQL.Text +
                'AND (' +
                '(OWNER_ROLE = CURRENT_ROLE)))'
          else SQL.Text:= SQL.Text +
                'AND (OWNER_USER = CURRENT_USER))';
        Prepare;
      end;

      with qryTreeSubChild do
      begin
        if Active then Active:= False;
        SQL.Text:= Format(SQLChildSelect,[TblName])
          //'SELECT ID, PARENT_ID, CAPTION, DESCRIPTION, OWNER_USER, OWNER_ROLE ' +
          //'FROM TBL_COMPLAINTS ' +
          //'WHERE ((DELETED_REC = 0) AND (PARENT_ID = :prmParentID) AND (DEPART_KIND = :prmDepartKind) '
          ;

        if IsAllowShowNonMyOwnedEntries
          then SQL.Text:= SQL.Text +
                'AND (' +
                '(OWNER_ROLE = CURRENT_ROLE)))'
          else SQL.Text:= SQL.Text +
                'AND (OWNER_USER = CURRENT_USER))';
        Prepare;
      end;

      //заполняем дерево root-узлами
      qryTreeRoot.First;

      while not qryTreeRoot.EOF do
      begin
        aNode:= VST.AddChild(nil);//добавляем root-узел
        aNodeData:= VST.GetNodeData(aNode);//получим ссылку на данные узла

        //присвоим данным значения
        with aNodeData^ do
        begin
          ID:= qryTreeRoot.FN('ID').AsInteger;
          ParentID:= qryTreeRoot.FN('PARENT_ID').AsInteger;
          StrCaption:= qryTreeRoot.FN('CAPTION').AsString;
          OwnerUser:= qryTreeRoot.FN('OWNER_USER').AsString;
          OwnerRole:= qryTreeRoot.FN('OWNER_ROLE').AsString;
        end;

        with qryTreeChild do
        begin
          if Active then Active:= False;
          ParamByName('prmParentID').AsInteger:= qryTreeRoot.FN('ID').AsInteger;
          ParamByName('prmDepartKind').AsString:= UTF8UpperCase(DepartmentKind);
          Active:= True;
        end;

        //заполняем дерево child-узлами
        if not qryTreeChild.IsEmpty then
        begin
          qryTreeChild.First;
          while not qryTreeChild.EOF do
          begin
            aChildNode:= VST.AddChild(aNode);//добавляем дочерний узел к aNode
            aNodeData:= VST.GetNodeData(aChildNode);//получаем ссылку на данные узла

            //присвоим данным значения
            with aNodeData^ do
            begin
              ID:= qryTreeChild.FN('ID').AsInteger;
              ParentID:= qryTreeChild.FN('PARENT_ID').AsInteger;;
              StrCaption:= qryTreeChild.FN('CAPTION').AsString;
              StrDescript:= qryTreeChild.FN('DESCRIPTION').AsString;
              OwnerUser:= qryTreeChild.FN('OWNER_USER').AsString;
              OwnerRole:= qryTreeChild.FN('OWNER_ROLE').AsString;
            end;

            with qryTreeSubChild do
            begin
              if Active then Active:= False;
              ParamByName('prmParentID').AsInteger:= qryTreeChild.FN('ID').AsInteger;
              ParamByName('prmDepartKind').AsString:= UTF8UpperCase(DepartmentKind);
              Active:= True;
            end;

            //заполняем дерево subchild-узлами
            if not qryTreeSubChild.IsEmpty then
            begin
              qryTreeSubChild.First;
              while not qryTreeSubChild.EOF do
              begin
                aSubChildNode:= VST.AddChild(aChildNode);//добавляем дочерний узел к aChildNode;
                aNodeData:= VST.GetNodeData(aSubChildNode);//получаем ссылку на данные узла

                //присвоим данным значения
                with aNodeData^ do
                begin
                  ID:= qryTreeSubChild.FN('ID').AsInteger;
                  ParentID:= qryTreeSubChild.FN('PARENT_ID').AsInteger;;
                  StrCaption:= qryTreeSubChild.FN('CAPTION').AsString;
                  StrDescript:= qryTreeSubChild.FN('DESCRIPTION').AsString;
                  OwnerUser:= qryTreeSubChild.FN('OWNER_USER').AsString;
                  OwnerRole:= qryTreeSubChild.FN('OWNER_ROLE').AsString;
                end;

                qryTreeSubChild.Next;
              end;{while not qryTreeSubChild.EOF do}
            end;{if not qryTreeChild.IsEmpty then}

            qryTreeChild.Next;
          end;{while not qryTreeChild.EOF do}
        end;{if not qryTreeChild.IsEmpty then}

        qryTreeRoot.Next;
      end;{while not qryTreeRoot.EOF do}
    except
      on E:EIBError do
      begin
        {$IFDEF MSWINDOWS}
        ErrMsg:= WinCPToUTF8(E.Message);
        {$ELSE}
        ErrMsg:= E.Message;
        {$ENDIF}

        qryTreeChild.Active:= False;
        QuestionDlg(MsgCaptErrorReadData,
                    Format(MsgTextControlErrorDetail,
                          [ErrMsg,
                          sLineBreak,
                          Self.UnitName,
                          sLineBreak,
                          qryTreeChild.Name]),
                    mtError,
                    [mrOK,CaptDlgBtnOK],
                    0
                    );
      end;
    end;
  finally
    VST.EndUpdate;
    FIsQryTreeChildUpdated:= qryTreeChild.Active;
  end;
end;

procedure TForm1.ActFrameCreateExecute(Sender: TObject);
var
  tmpFr: TfrEdit = nil;
  idx: PtrInt = -1; //индекс вставленного элемента
  ErrMsg: String = '';
begin
  if not Assigned(PnlObjList) then Exit;

  if (PnlObjList.Count > MaxPnlCount) then Exit; //если превысили лимит
  if (PnlObjList.Count > 1) and (EditMode = emEdit) then Exit;

  Self.BeginFormUpdate;
  try
    try
      tmpFr:= TfrEdit.Create(Self);
      idx:= PnlObjList.Add(tmpFr);

      with tmpFr do
      begin
        Parent:= scrboxFr;

        if (idx = 0) //первый элемент списка (т.е. самый верхний фрэйм на форме)
        then
          begin
            AnchorSideTop.Side:= asrTop;
            AnchoringTopControl:= Parent;
          end
        else
          begin
            AnchorSideTop.Side:= asrBottom;
            AnchoringTopControl:= PnlObjList.Items[Pred(idx)];
          end;

        IsAddMode:= (EditMode = emAdd);
        Visible:= True;

        {$IFDEF MSWINDOWS}
        btnRigth.Enabled:= ((idx <> 0) and IsAddMode);
        btnLeft.Enabled:= ((idx < Pred(MaxPnlCount)) and IsAddMode);
        {$ELSE}
        btnLeft.Enabled:= ((idx <> 0) and IsAddMode);
        btnRigth.Enabled:= ((idx < Pred(MaxPnlCount)) and IsAddMode);
        {$ENDIF}
      end;

    except
      on E:EIBError do
      begin
        {$IFDEF MSWINDOWS}
          ErrMsg:= WinCPToUTF8(E.Message);
        {$ELSE}
          ErrMsg:= E.Message;
        {$ENDIF}

        if Assigned(tmpFr) then FreeAndNil(tmpFr);
        QuestionDlg(MsgCaptErrorFrameCreating,
                    Format(MsgTextControlErrorDetail,
                          [ErrMsg,
                          sLineBreak,
                          Self.UnitName,
                          sLineBreak,
                          tmpFr.Name]),
                    mtError,
                    [mrOK,CaptDlgBtnOK],
                    0
                    );

        Exit;
      end;
    end;
  finally
    Self.EndFormUpdate;
    scrboxFr.ScrollInView(PnlObjList.Items[idx]);
    PnlObjList.Items[idx].edtEdit.SetFocus;
  end;
end;

procedure TForm1.ActFrameRemoveExecute(Sender: TObject);
var
  idx: PtrInt = -1;//индекс текущей (удаляемой) панели
  SendersPar: TfrEdit = nil;//родитель кнопки-сендера
  ErrMsg: String = '';
begin
  if (PnlObjList.Count < 2) then Exit;
  if not TObject(Sender).InheritsFrom(TButton) then Exit;

  Self.BeginFormUpdate;
  try
    try
      SendersPar:= TfrEdit(TButton(Sender).Parent);
      idx:= PnlObjList.IndexOf(SendersPar);//получаем индекс удаляемой панели

      if not SendersPar.Equals(PnlObjList.Last) //панель посередине
      then
        //привязываем (относительно текущей панели) верхний край ниже лежащей панели к выше лежащей
        PnlObjList.Items[Succ(idx)].AnchorSideTop.Control:= PnlObjList.Items[Pred(idx)];


      PnlObjList.Delete(idx);
      PnlObjList.TrimExcess;//приводим емкость в соответствии с кол-вом элементов
    except
      on E:Exception do
      begin
        {$IFDEF MSWINDOWS}
          ErrMsg:= WinCPToUTF8(E.Message);
        {$ELSE}
          ErrMsg:= E.Message;
        {$ENDIF}
        QuestionDlg(MsgCaptErrorFrameDeleting,
                    Format(MsgTextControlErrorDetail,
                          [ErrMsg,
                          sLineBreak,
                          Self.UnitName,
                          sLineBreak,
                          PnlObjList.Items[idx].Name]),
                    mtError,
                    [mrOK,CaptDlgBtnOK],
                    0
                    );
      end;
    end;

  finally
    Self.EndFormUpdate;
    scrboxFr.ScrollInView(PnlObjList.Last);
    PnlObjList.Last.edtEdit.SetFocus;
  end;
end;

procedure TForm1.ActqryTreeRootOpenExecute(Sender: TObject);
var
  ErrMsg: String = '';
begin
  with qryTreeRoot do
  begin
    try
      if Active then Active:= False;
      SQL.Text:= Format(SQLRootSelect,[TblName])
        //'SELECT ID, PARENT_ID, CAPTION, DESCRIPTION, OWNER_USER, ' +
        //  'OWNER_ROLE ' +
        //'FROM TBL_COMPLAINTS ' +
        //'WHERE ((PARENT_ID = :prmParentID) AND (DELETED_REC = 0) AND (DEPART_KIND = :prmDepartKind) '
        ;

      if IsAllowShowNonMyOwnedEntries
        then SQL.Text:= SQL.Text +
                         'AND (' +
                         '(OWNER_ROLE = CURRENT_ROLE)))'
        else SQL.Text:= SQL.Text +
                         'AND (OWNER_USER = CURRENT_USER))';

      Prepare;
      ParamByName('prmDepartKind').AsString:= UTF8UpperCase(DepartmentKind);
      ParamByName('prmParentID').AsInteger:= FParentID;
      Active:= True;
    except
      on E:EIBError do
      begin
        {$IFDEF MSWINDOWS}
        ErrMsg:= WinCPToUTF8(E.Message);
        {$ELSE}
        ErrMsg:= E.Message;
        {$ENDIF}

        Active:= False;
        QuestionDlg(MsgCaptErrorReadData,
                    Format(MsgTextControlErrorDetail,
                          [ErrMsg,
                          sLineBreak,
                          Self.UnitName,
                          sLineBreak,
                          Name]),
                    mtError,
                    [mrOK,CaptDlgBtnOK],
                    0
                    );
      end;
    end;

    FIsQryTreeRootUpdated:= Active;
  end;
end;

procedure TForm1.ActReportExecute(Sender: TObject);
var
  SB: TStringBuilder = nil;
  Node: PVirtualNode = nil;
  ChildNode: PVirtualNode = nil;
  SubChildNode: PVirtualNode = nil;
  NodeData: PTreeData = nil;
  tmpStr: RawByteString = '';

  function GetItemsValue(Sender: TComboBox): RawByteString;
  const
    cValueEmpty = '';
    cValueSpace = ' ';
    cValueLineBreak = sLineBreak;
  begin
    Result:= cValueEmpty;
    if (TComboBox(Sender).Items.Count = 0) then Exit;

    case TComboBox(Sender).ItemIndex of
      1: Result:= cValueSpace;
      2: Result:= cValueLineBreak;
    end;
  end;
begin
  Memo1.Clear;
  if VST.IsEmpty then Exit;
  SB:= TStringBuilder.Create;
  try
    SB.Clear;
    Node:= VST.GetFirst(True);
    while Assigned(Node) do
    begin
      if (Node^.CheckState <> csUncheckedNormal) then //root и/или детки помечены
      begin
        if chbIncludeRR.Checked then
        begin
          NodeData:= VST.GetNodeData(Node);
          SB.AppendFormat('%s%s%s',[
                          NodeData^.StrCaption,
                          UTF8Trim(edtR.Text),
                          GetItemsValue(cbbR)
                                    ]);
        end;

        if (Node^.ChildCount > 0) then
        begin
          ChildNode:= Node^.FirstChild;
          while Assigned(ChildNode) do
          begin
            if (ChildNode^.CheckState <> csUncheckedNormal) then
            begin
              if chbIncludeCh.Checked then
              begin
                NodeData:= VST.GetNodeData(ChildNode);
                if (rgCh.ItemIndex = 0)
                  then tmpStr:= NodeData^.StrCaption
                  else tmpStr:= NodeData^.StrDescript;

                SB.AppendFormat('%s%s%s',[
                                tmpStr,
                                UTF8Trim(edtCh.Text),
                                GetItemsValue(cbbCh)
                                          ]);
              end;

              if (ChildNode^.ChildCount > 0) then
              begin
                SubChildNode:= ChildNode^.FirstChild;

                while Assigned(SubChildNode) do
                begin
                  if (SubChildNode^.CheckState = csCheckedNormal) then
                  begin
                    NodeData:= VST.GetNodeData(SubChildNode);

                    if (rgSch.ItemIndex = 0)
                      then tmpStr:= NodeData^.StrCaption
                      else tmpStr:= NodeData^.StrDescript;

                    SB.AppendFormat('%s%s%s',[
                                    tmpStr,
                                    UTF8Trim(edtSChSCh.Text),
                                    GetItemsValue(cbbSChSCh)
                                              ]);
                  end;
                  SubChildNode:= SubChildNode^.NextSibling;
                end;{while Assigned(SubChildNode) do}
              end;

              //добавляем разделитель между child-узлами
              if chbIncludeCh.Checked then
                SB.AppendFormat('%s%s',[UTF8Trim(edtChCh.Text),GetItemsValue(cbbChCh)]);

            end;{(ChildNode^.CheckState <> csUncheckedNormal)}

            ChildNode:= ChildNode^.NextSibling;
          end;{ while Assigned(ChildNode) do} //(NodeLevel = 1)
        end;

        //добавляем разделитель между root-узлами
        if chbIncludeRR.Checked then
          SB.AppendFormat('%s%s',[UTF8Trim(edtRR.Text),GetItemsValue(cbbRR)]);
      end;{(Node^.CheckState <> csUncheckedNormal)}
      Node:= Node^.NextSibling;
    end;{while Assigned(Node) do} //(NodeLevel = 0)

    Memo1.Text:= SB.ToString;
  finally
    FreeAndNil(SB);
  end;
end;

procedure TForm1.ActRootAddEditExecute(Sender: TObject);
var
  tmpFrm: TfrmRootEntryEdit = nil;
  id: PtrInt = -1;
  ErrMsg: String = '';
  OldDataNodeID: PtrInt = -1;//значение LastSelectedNodeID до начала манипуляций
begin
  if not (TButton(Sender).Equals(btnRootAdd) or TButton(Sender).Equals(btnRootEdit)) then Exit;
  if not qryTreeRoot.Active then Exit;

  //запрещаем редактировать "чужие" узлы
  if TButton(Sender).Equals(btnRootEdit) then
    if qryTreeRoot.Locate('CAPTION', cbbRoot.Items[cbbRoot.ItemIndex],[]) then
    begin
      if (not IsAllowEditNonMyOwnedEntries
            and (qryTreeRoot.FN('OWNER_USER').AsString <> CurUser)) then
      begin
        QuestionDlg(MsgCaptErrorAccessDenied,
                    CaptTreeNodeEditDeniedAccess,
                    mtWarning,
                    [mrOK,CaptDlgBtnAllRight],
                    0
                    );
        Exit;
      end;
    end;

  OldDataNodeID:= LastSelectedNodeID;//сохраняем значение последнего выделенного узла

  tmpFrm:= TfrmRootEntryEdit.Create(Self);
  try
    if TButton(Sender).Equals(btnRootAdd)
    then
      begin
        EditMode:= emAdd;
        tmpFrm.edtRootCaption.Clear;
      end
    else
      begin
        EditMode:= emEdit;
        tmpFrm.edtRootCaption.Text:= cbbRoot.Items[cbbRoot.ItemIndex];
      end;

    tmpFrm.ShowModal;

    if (tmpFrm.ModalResult = mrOK) then
    begin
      try
        TransW.StartTransaction;
        with sqlExecTree do
        begin
          if Open then Close;
          case EditMode of
            emAdd:
              begin
                SQL.Text:= Format(SQLRootInsert,[TblName])
                  //'INSERT INTO TBL_COMPLAINTS (CAPTION, DEPART_KIND) ' +
                  //'VALUES (:prmCaption, :prmDepartKind) ' +
                  //'RETURNING ID'
                  ;
                Prepare;
              end;
            emEdit:
              begin
                SQL.Text:= Format(SQLRootUpdate,[TblName])
                //'UPDATE TBL_COMPLAINTS ' +
                //'SET CAPTION = :prmCaption ' +
                //'WHERE (ID = :prmID)'
                ;
                Prepare;

                if qryTreeRoot.Locate('CAPTION',cbbRoot.Items[cbbRoot.ItemIndex],[])
                  then id:= qryTreeRoot.FN('ID').AsInteger;
                ParamByName('prmID').AsInteger:= id;
              end;
          end;

          ParamByName('prmCaption').AsString:= UTF8Trim(tmpFrm.edtRootCaption.Text);

          if (EditMode = emAdd)
            then ParamByName('prmDepartKind').AsString:= UTF8UpperCase(DepartmentKind);

          ExecQuery;

          FLastSelectedNodeID:= id;

          if (EditMode = emAdd) then id:= FieldByName('ID').AsInteger;
        end;{with sqlExecTree do...}

        TransW.Commit;
        FIsQryTreeRootUpdated:= True;//взводим флаг

        ActqryTreeRootOpenExecute(Sender);//перечитываем набор
        ActFillcbbRootExecute(Sender);//перезаполняем список

        //позиционируем список
        if qryTreeRoot.Locate('ID',id,[])
          then cbbRoot.ItemIndex:= cbbRoot.Items.IndexOf(qryTreeRoot.FN('CAPTION').AsString)
          else cbbRoot.ItemIndex:= -1;
      except
        on E:EIBError do
        begin
          TransW.Rollback;
          FLastSelectedNodeID:= OldDataNodeID;//возвращаем "старое" значение
          FIsQryTreeRootUpdated:= False;//сбрасываем флаг

          {$IFDEF MSWINDOWS}
          ErrMsg:= WinCPToUTF8(E.Message);
          {$ELSE}
          ErrMsg:= E.Message;
          {$ENDIF}


          QuestionDlg(MsgCaptErrorModifyData,
                      Format(MsgTextControlErrorDetail,
                            [ErrMsg,
                            sLineBreak,
                            Self.UnitName,
                            sLineBreak,
                            sqlExecTree.Name]),
                      mtError,
                      [mrOK,CaptDlgBtnOK],
                      0
                      );
        end;
      end;
    end;{tmpFrm.ModalResult = mrOK}
  finally
    FreeAndNil(tmpFrm);
  end;
end;

procedure TForm1.ActEntryDelExecute(Sender: TObject);
var
  aNode: PVirtualNode = nil;
  aNodeLvl: PtrInt = -1;
  aNodeData: PTreeData = nil;
  ErrMsg: String = '';
  OldDataNodeID: PtrInt = -1;//значение LastSelectedNodeID до начала манипуляций
begin
  //не редактируем множественное выделение
  if (VST.SelectedCount > 1) then
  begin
    QuestionDlg(MsgCaptWarningData,
                CaptTreeNodeCannotDeleteMultiSelect,
                mtWarning,
                [mrOK,CaptDlgBtnAllRight],
                0
                );
    Exit;
  end;

  aNode:= VST.GetFirstSelected(False);//первый выделенный узел(дочерние выделенные не учитываются)

  if not Assigned(aNode) then
  begin
    QuestionDlg(MsgCaptWarningData,
                CaptTreeNodeSelectRequired,
                mtWarning,
                [mrOK,CaptDlgBtnAllRight],
                0
                );
    Exit;
  end;

  if Assigned(aNode) then aNodeData:= VST.GetNodeData(aNode) ;

  //запрещаем редактировать "чужие" узлы
  if (not IsAllowEditNonMyOwnedEntries and (aNodeData^.OwnerUser <> CurUser)) then
  begin
    QuestionDlg(MsgCaptErrorAccessDenied,
                CaptTreeNodeEditDeniedAccess,
                mtWarning,
                [mrOK,CaptDlgBtnAllRight],
                0
                );
    Exit;
  end;

  OldDataNodeID:= LastSelectedNodeID;//сохраняем значение последнего выделенного узла
  aNodeLvl:= VST.GetNodeLevel(aNode);

  with sqlExecTree do begin
    try
      TransW.StartTransaction;
      if Open then Close;
      SQL.Text:= Format(SQLChildDelete,[TblName])
            //'UPDATE TBL_COMPLAINTS ' +
            //'SET DELETED_REC = 1 ' +
            //'WHERE (ID = :prmID)'
            ;
      Prepare;
      ParamByName('prmID').AsInteger:= aNodeData^.ID;
      ExecQuery;
      TransW.Commit;

      FLastSelectedNodeID:= aNodeData^.ID;


      //обновляем набор root, если удаляли его
      if (aNodeLvl = 0) then ActqryTreeRootOpenExecute(Sender);

      ActFillVSTExecute(Sender);//перезаполняем дерево
      chbExpandChange(Sender);
      ActRefreshVTVExecute(Sender);
    except
      on E:EIBError do
      begin
        TransW.Rollback;
        FLastSelectedNodeID:= OldDataNodeID;//возвращаем "старое" значение

        {$IFDEF MSWINDOWS}
        ErrMsg:= WinCPToUTF8(E.Message);
        {$ELSE}
        ErrMsg:= E.Message;
        {$ENDIF}


        QuestionDlg(MsgCaptErrorModifyData,
                    Format(MsgTextControlErrorDetail,
                          [ErrMsg,
                          sLineBreak,
                          Self.UnitName,
                          sLineBreak,
                          Name]),
                    mtError,
                    [mrOK,CaptDlgBtnOK],
                    0
                    );
      end;
    end;
  end;{with sqlExecTree do}
end;

procedure TForm1.ActEditNodeFinishExecute(Sender: TObject);
var
  i: PtrInt = 0;
begin
  for i := 0 to Pred(pnlBtn.ControlCount) do
    if pnlBtn.Controls[i].InheritsFrom(TButton) then
      TButton(pnlBtn.Controls[i]).Visible:= (TButton(pnlBtn.Controls[i]).Tag = -1);

  pnlFrame.Visible:= False;
  Panel1.Visible:= True;
  splMemo.Visible:= True;
  Memo1.Visible:= not pnlFrame.Visible;
  VST.Visible:= not pnlFrame.Visible;
  pnlSettings.Visible:= (not pnlFrame.Visible) and (tboxSettings.Checked);
  tboxSettings.Visible:= not pnlFrame.Visible;
  splVST.Visible:= not pnlFrame.Visible;
  pnlSearch.Visible:= IsShowPnlSearch and not pnlFrame.Visible;
end;

procedure TForm1.ActEditNodeStartExecute(Sender: TObject);
var
  i: PtrInt = 0;
begin
  for i := 0 to Pred(pnlBtn.ControlCount) do
    if pnlBtn.Controls[i].InheritsFrom(TButton) then
      TButton(pnlBtn.Controls[i]).Visible:= (TButton(pnlBtn.Controls[i]).Tag = -2);

  pnlFrame.Visible:= True;
  Panel1.Visible:= False;
  splMemo.Visible:= False;
  Memo1.Visible:= not pnlFrame.Visible;
  VST.Visible:= not pnlFrame.Visible;
  pnlSettings.Visible:= not pnlFrame.Visible;
  tboxSettings.Visible:= not pnlFrame.Visible;
  splVST.Visible:= not pnlFrame.Visible;
  pnlSearch.Visible:= IsShowPnlSearch and not pnlFrame.Visible;
end;

procedure TForm1.ActRefreshVTVExecute(Sender: TObject);
var
  aNode: PVirtualNode = nil;
begin
  if VST.IsEmpty then Exit;

  aNode:= GetNodeByID(VST,LastSelectedNodeID);

  VST.BeginUpdate;
  try
    if not Assigned(aNode) then
    begin
      //ищем родителя
      aNode:= GetNodeByID(VST, LastSelNodeParentID);

      if not Assigned(aNode)
        then aNode:= VST.GetFirst(True) //берем первый попавшийся
        else //берем первого ребенка взамен удаленного, если есть
          if (aNode^.ChildCount > 0) then aNode:= aNode^.FirstChild;
    end;

    VST.Selected[aNode]:= True;//выделяем узел в дереве;
    aNode^.States:= aNode^.States + [vsExpanded];//раскрываем, если узел в дереве "схлопнут"

    VST.ScrollIntoView(aNode,True,False);//пытаемся скроллировать выделение в центре экрана
  finally
    VST.EndUpdate;
  end;
end;

procedure TForm1.ActSearchNodeExecute(Sender: TObject);
var
  aNode: PVirtualNode = nil;
  aNodeData: PTreeData = nil;
  aNodeSearchDirect: TNodeSearchDirection = nsdSearchDown;
  aSearchDataField: TSearchDataField = sdfCaption;
  aSearchForText: String = '';
  aSearchInText: String = '';

  procedure FindChildNode(Sender: PVirtualNode;
                          NodeSearchDirect: TNodeSearchDirection = nsdSearchDown;
                          SearchForText: String = '';
                          SearchDataField: TSearchDataField = sdfCaption);
  var
    aNode: PVirtualNode = nil;
    aNodeData: PTreeData = nil;
    aSearchInText: String = '';
  begin
    if (UTF8Length(SearchForText) = 0) then Exit;

    aNode:= PVirtualNode(Sender)^.FirstChild;

    while Assigned(aNode) do
    begin
      aNodeData:= VST.GetNodeData(aNode);

      case SearchDataField of
        sdfCaption: aSearchInText:= aNodeData^.StrCaption;
        sdfDescript: aSearchInText:= aNodeData^.StrDescript;
      end;

      //если регистроНЕЗАВИСИМЫЙ поиск
      if not IsCaseSensitiveSearch
      then
        begin
          if UTF8Pos(
                      UTF8LowerCase(SearchForText),
                      UTF8LowerCase(aSearchInText)
                    ) > 0
            then VST.AddToSelection(aNode);
        end
      else
        begin
          if UTF8Pos(SearchForText, aSearchInText) > 0
            then VST.AddToSelection(aNode);
        end;

        //если узел выделен поиском и имеет родителя,то
        if ((VST.Selected[aNode]) and Assigned(aNode^.Parent)) then
          //если родитель "свернут", то разворачиваем родителя
          if not (vsExpanded in aNode^.States) then
            //(aNode^.Parent)^.States:= (aNode^.Parent)^.States + [vsExpanded];
            Include((aNode^.Parent)^.States, vsExpanded);
            //VST.Expanded[aNode^.Parent]:= True;

      //если выделяем только один найденный узел, а их уже больше
      //if  not (nsoSelectAllNodes in NodeSearchOption) and (VST.SelectedCount > 0) then Break;
      if  not IsSelectAllFoundNodes and (VST.SelectedCount > 0) then
      begin
        //снимаем выделение с предыдущего узла, если есть
        if VST.Selected[LastFoundNode] then VST.RemoveFromSelection(FLastFoundNode);//снимаем выделение с предыдущего узла, если есть
        FLastFoundNode:= aNode;
        Break;
      end;

      if VST.HasChildren[aNode] then FindChildNode(aNode, NodeSearchDirect, SearchForText,SearchDataField);

      //переход к следующему
      if not IsSelectAllFoundNodes and (NodeSearchDirect = nsdSearchUp)
        then aNode:= aNode^.PrevSibling //снизу
        else aNode:= aNode^.NextSibling;//сверху
    end;
  end;

  procedure FindParentNode(Sender: PVirtualNode;
                          NodeSearchDirect: TNodeSearchDirection = nsdSearchDown;
                          SearchForText: String = '';
                          SearchDataField: TSearchDataField = sdfCaption);
  var
    aNode: PVirtualNode = nil;
    aNodeData: PTreeData = nil;
    aSearchInText: String = '';
  begin
    if (UTF8Length(SearchForText) = 0) then Exit;
    {!!! поработать на функцией !!!}
    aNode:= PVirtualNode(Sender)^.Parent;

    while Assigned(aNode) do
    begin
      aNodeData:= VST.GetNodeData(aNode);

      case SearchDataField of
        sdfCaption: aSearchInText:= aNodeData^.StrCaption;
        sdfDescript: aSearchInText:= aNodeData^.StrDescript;
      end;

      //если регистроНЕЗАВИСИМЫЙ поиск
      if not IsCaseSensitiveSearch
      then
        begin
          if UTF8Pos(
                      UTF8LowerCase(SearchForText),
                      UTF8LowerCase(aSearchInText)
                    ) > 0
            then VST.AddToSelection(aNode);
        end
      else
        begin
          if UTF8Pos(SearchForText, aSearchInText) > 0
            then VST.AddToSelection(aNode);
        end;

        //если узел выделен поиском и имеет родителя,то
        if ((VST.Selected[aNode]) and Assigned(aNode^.Parent)) then
          //если родитель "свернут", то разворачиваем родителя
          if not (vsExpanded in aNode^.States) then
            //(aNode^.Parent)^.States:= (aNode^.Parent)^.States + [vsExpanded];
            Include((aNode^.Parent)^.States, vsExpanded);
            //VST.Expanded[aNode^.Parent]:= True;

      //если выделяем только один найденный узел, а их уже больше
      //if  not (nsoSelectAllNodes in NodeSearchOption) and (VST.SelectedCount > 0) then Break;
      if  not IsSelectAllFoundNodes and (VST.SelectedCount > 0) then
      begin
        //снимаем выделение с предыдущего узла, если есть
        if VST.Selected[LastFoundNode] then VST.RemoveFromSelection(FLastFoundNode);//снимаем выделение с предыдущего узла, если есть
        FLastFoundNode:= aNode;
        Break;
      end;

      if VST.HasChildren[aNode] then FindChildNode(aNode, NodeSearchDirect, SearchForText,SearchDataField);

      //переход к следующему
      if not IsSelectAllFoundNodes and (NodeSearchDirect = nsdSearchUp)
        then aNode:= aNode^.PrevSibling //снизу
        else aNode:= aNode^.NextSibling;//сверху
    end;
  end;
begin
  if VST.IsEmpty then Exit;

  aSearchForText:= UTF8Trim(edtSearchText.Text);

  if (UTF8Length(aSearchForText) = 0) then Exit;

  if TButton(Sender).Equals(btnSearchUp)
    then aNodeSearchDirect:= nsdSearchUp
    else aNodeSearchDirect:= nsdSearchDown;

  //снимаем выделение при множественном выделении
  if (VST.SelectedCount > 0) and IsSelectAllFoundNodes then VST.ClearSelection;

  //if IsSelectAllFoundNodes
  //  then aNodeSearchOption:= aNodeSearchOption + [nsoSelectAllNodes]
  //  else aNodeSearchOption:= aNodeSearchOption - [nsoSelectAllNodes];
  //
  //if chbSearchCaseSensitive.Checked
  //  then aNodeSearchOption:= aNodeSearchOption + [nsoSearchCaseSensitive]
  //  else aNodeSearchOption:= aNodeSearchOption - [nsoSearchCaseSensitive];

  { #todo : переделать на выборочный поиск с определенного узла в зависимости от того, разрешено ли выделение ВСЕХ узлов иди нет }

  //если множественный выбор узлов разрешен
  if IsSelectAllFoundNodes
    then aNode:= VST.GetFirst(False)//поиск с начала дерева
    else
      begin
        if Assigned(LastFoundNode) //поиск продолжается
          then //начинаем с последнего найденного
            begin
              if VST.HasChildren[LastFoundNode] //имеет детишек
                then
                  //берем первого ребенка
                  case aNodeSearchDirect of
                    nsdSearchDown: aNode:= LastFoundNode^.FirstChild;//сверху
                    nsdSearchUp: aNode:= LastFoundNode^.LastChild; //снизу
                  end
                else //следующего равного
                  case aNodeSearchDirect of
                    nsdSearchDown: aNode:= LastFoundNode^.NextSibling;//сверху
                    nsdSearchUp: aNode:= LastFoundNode^.PrevSibling; //снизу
                  end
            end
          else
            aNode:= VST.GetFirst(False);//начинаем с первого
      end;

  VST.BeginUpdate;
  try
    while Assigned(aNode) do
    begin
      aNodeData:= VST.GetNodeData(aNode);

      case rgSearchField.ItemIndex of
        0:
          begin
            aSearchDataField:= sdfCaption;//ищем по Data^.StrCaption
            aSearchInText:= aNodeData^.StrCaption;
          end;
        1:
          begin
            aSearchDataField:= sdfDescript;//ищем по Data^.StrDescript
            aSearchInText:= aNodeData^.StrDescript;
          end;
      end;

      //если регистроНЕЗАВИСИМЫЙ поиск
      if not IsCaseSensitiveSearch then
      begin
          if UTF8Pos(
                      UTF8LowerCase(aSearchForText),
                      UTF8LowerCase(aSearchInText)
                    ) > 0
            then VST.AddToSelection(aNode);
        end
      else
        begin
          if UTF8Pos(aSearchForText, aSearchInText) > 0
            then VST.AddToSelection(aNode)
            //VST.Selected[aNode]:= True
            ;
        end;

      //if not (vsExpanded in aNode^.States)
      //  then ShowMessage('свернут')
      //  else ShowMessage('раскрыт');
      //
      //Break;


      //если узел выделен поиском и имеет родителя,то
      if ((VST.Selected[aNode]) and Assigned(aNode^.Parent)) then
        //если родитель "свернут", то разворачиваем родителя
        if not (vsExpanded in aNode^.States) then
          //(aNode^.Parent)^.States:= (aNode^.Parent)^.States + [vsExpanded];
          //Include(PVirtualNode(aNode^.Parent)^.States, vsExpanded);
          VST.Expanded[PVirtualNode(aNode^.Parent)]:= True;


      //если выделяем только один найденный узел, а их уже больше
      if  not IsSelectAllFoundNodes and (VST.SelectedCount > 0) then
      begin
        if VST.Selected[LastFoundNode] then VST.RemoveFromSelection(FLastFoundNode);//снимаем выделение с предыдущего узла, если есть
        FLastFoundNode:= aNode;
        Break;
      end;

      if VST.HasChildren[aNode]
        then FindChildNode(aNode, aNodeSearchDirect, aSearchForText, aSearchDataField);

      //переход к следующему
      if IsSelectAllFoundNodes
      then
        aNode:= aNode^.NextSibling
      else
        begin
          case aNodeSearchDirect of
            nsdSearchDown: aNode:= aNode^.NextSibling; //сверху
              nsdSearchUp: aNode:= aNode^.PrevSibling; //снизу
          end;
        end;
    end;
  finally
    VST.EndUpdate;
  end;
end;

procedure TForm1.btnDelesectClick(Sender: TObject);
var
  aNode: PVirtualNode = nil;
begin
  aNode:= VST.GetFirstSelected(False);
  if (Assigned(aNode) and (VST.SelectedCount > 0)) then VST.RemoveFromSelection(aNode);
  //VST.Selected[aNode]:= False;
end;

procedure TForm1.btnShowClick(Sender: TObject);
const
    OutputStr = '-%s (%s)';
var
  aNode: PVirtualNode = nil;
  aChildNode: PVirtualNode = nil;
begin

  if VST.IsEmpty then Exit;

  aNode:= VST.GetFirstChecked(csCheckedNormal,True);//получаем первый отмеченый узел в дереве

  if not Assigned(aNode) then
  begin
    QuestionDlg(MsgCaptWarningData,
                CaptTreeNodeSelectRequired,
                mtInformation,
                [mrOK,CaptDlgBtnAllRight],
                0
                );
    Exit;
  end;

  Memo1.Clear;

  aNode:= VST.GetFirst(False);

  while Assigned(aNode) do
  begin
    //if (aNode^.ChildCount > 0) then
    if (VST.HasChildren[aNode]) then
    begin
      aChildNode:= aNode^.FirstChild;

      while Assigned(aChildNode) do
      begin
        if (aChildNode^.CheckState = csCheckedNormal)//встретился первый "чекнутый" child
        then
          begin
            if IsRootTextInclude  then
              Memo1.Lines.Add(PTreeData(VST.GetNodeData(aNode))^.StrCaption);

            aChildNode:= aNode^.FirstChild;

            //перебором добавляем текст "чекнутых" child'ов
            while Assigned(aChildNode) do
            begin
              if (aChildNode^.CheckState = csCheckedNormal) then
                Memo1.Lines.Add(Format(OutputStr,
                                 [PTreeData(VST.GetNodeData(aChildNode))^.StrCaption,
                                 PTreeData(VST.GetNodeData(aChildNode))^.StrDescript]));

               aChildNode:= aChildNode^.NextSibling;
            end;

            Break;//переходим к следующему root'у
          end
        else
          aChildNode:= aChildNode^.NextSibling;
      end;
    end;
    aNode:= aNode^.NextSibling;
  end;{ while Assigned(aNode) do}
end;

procedure TForm1.chbAllowEditNonOwnEntriesChange(Sender: TObject);
begin
  FIsAllowEditNonMyOwnedEntries:= chbAllowEditNonOwnEntries.Checked
                                    and IsAllowShowNonMyOwnedEntries;
end;

procedure TForm1.chbAllowShowNonOwnEntriesChange(Sender: TObject);

begin
  FIsAllowShowNonMyOwnedEntries:= chbAllowShowNonOwnEntries.Checked;
  chbAllowEditNonOwnEntries.Enabled:= IsAllowShowNonMyOwnedEntries;
  chbAllowEditNonOwnEntriesChange(Sender);

  if not qryTreeRoot.Active then Exit;

  //запоминаем ID выделенного узла
  if (VST.SelectedCount > 0) then
  begin
    FLastSelectedNodeID:= PTreeData(VST.GetNodeData(VST.GetFirstSelected(True)))^.ID;
  end;
  ActqryTreeRootOpenExecute(Sender);
  ActFillVSTExecute(Sender);
  chbExpandChange(Sender);
  ActRefreshVTVExecute(Sender);
end;

procedure TForm1.chbAutoExpandChange(Sender: TObject);
begin
  if chbAutoExpand.Checked
    then
      VST.TreeOptions.AutoOptions:= VST.TreeOptions.AutoOptions + [toAutoExpand]
    else
      VST.TreeOptions.AutoOptions:= VST.TreeOptions.AutoOptions - [toAutoExpand];
end;

procedure TForm1.chbExpandChange(Sender: TObject);
begin
  FIsAllRootNodesExpand:= chbExpand.Checked;

  if IsAllRootNodesExpand
    then VST.FullExpand(nil)
    else VST.FullCollapse(nil);
end;

procedure TForm1.chbHeaderShowChange(Sender: TObject);
begin
  FIsShowColumnHeader:= chbHeaderShow.Checked;

  if IsShowColumnHeader
    then VST.Header.Options:= VST.Header.Options + [hoVisible]
    else VST.Header.Options:= VST.Header.Options - [hoVisible];
end;

procedure TForm1.chbIncludeChChange(Sender: TObject);
begin
  Label3.Enabled:= chbIncludeCh.Checked;
  edtCh.Enabled:= chbIncludeCh.Checked;
  cbbCh.Enabled:= chbIncludeCh.Checked;

  Label4.Enabled:= chbIncludeCh.Checked;
  edtChCh.Enabled:= chbIncludeCh.Checked;
  cbbChCh.Enabled:= chbIncludeCh.Checked;
  rgCh.Enabled:= chbIncludeCh.Checked;
end;

procedure TForm1.chbIncludeRRChange(Sender: TObject);
begin
  Label1.Enabled:= chbIncludeRR.Checked;
  edtR.Enabled:= chbIncludeRR.Checked;
  cbbR.Enabled:= chbIncludeRR.Checked;

  Label2.Enabled:= chbIncludeRR.Checked;
  edtRR.Enabled:= chbIncludeRR.Checked;
  cbbRR.Enabled:= chbIncludeRR.Checked;
end;

procedure TForm1.chbRootHasCheckboxChange(Sender: TObject);
var
  aNode: PVirtualNode = nil;
begin
  FIsRootHasCheckBox:= chbRootHasCheckbox.Checked;

  if VST.IsEmpty then Exit;
  aNode:= VST.GetFirst(False);

  VST.BeginUpdate;
  try
    while Assigned(aNode) do
    begin
      VST.ReinitNode(aNode,False);//перерисовываем узел и обновляем в нем Data
      aNode:= aNode^.NextSibling;
    end;
  finally
    VST.EndUpdate;
    VST.Refresh;
  end;
end;

procedure TForm1.chbRootTextIncludeChange(Sender: TObject);
begin
  FIsRootTextInclude:= chbRootTextInclude.Checked;
end;

procedure TForm1.chbSearchCaseSensitiveChange(Sender: TObject);
begin
  FIsCaseSensitiveSearch:= chbSearchCaseSensitive.Checked;
end;

procedure TForm1.chbSelectAllFoundNodesChange(Sender: TObject);
begin
  FIsSelectAllFoundNodes:= chbSelectAllFoundNodes.Checked;
  btnSearchUp.Enabled:= not IsSelectAllFoundNodes;
  btnSearchDown.Enabled:= not IsSelectAllFoundNodes;
end;

procedure TForm1.chbShowSearchBoxChange(Sender: TObject);
begin
  FIsShowPnlSearch:= chbShowSearchBox.Checked;
  pnlSearch.Visible:= IsShowPnlSearch;
  chbSelectAllFoundNodesChange(Sender);
  chbSearchCaseSensitiveChange(Sender);
end;

procedure TForm1.edtSearchTextChange(Sender: TObject);
begin
  FLastFoundNode:= nil;
  if (UTF8Trim(edtSearchText.Text) = '') then Exit;
end;

procedure TForm1.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  if Assigned(PnlObjList) then PnlObjList.Free;
end;

procedure TForm1.FormCreate(Sender: TObject);
const
  Cnt = 1;
  cShortCapt = 'Краткое описание';
  cLongCapt = 'Подробное описание';
var
  i: PtrInt = -1;
  SomeCheckBox: TCheckBox = nil;
  chbWidth: LongInt = 0;
begin
  with VST.Header do
  begin
    Options:= Options
    + [hoVisible] //показываем заголовки
    //+ [hoAutoResize]
    //+ [hoAutoSpring]
    + [hoFullRepaintOnResize]
    + [hoColumnResize]
    + [hoShowHint]
    ;

    //Style:= hsPlates;

    for i:= 0 to Cnt do
    begin
      Columns.Add;
      Columns[i].Alignment:= taLeftJustify;
      case i of
        0:
          begin
            Columns[i].Options:= Columns[i].Options
                              + [coSmartResize, coResizable]
                              - [coDisableAnimatedResize]
                              ;
            Columns[i].Text:= CaptTreeNodeColumnNameCaption;
          end;
        1:
          begin
            Columns[i].Text:= CaptTreeNodeColumnNameDescript;
          end;
      end;
      Columns[i].MinWidth:= 100;
      Columns[i].Width:= 200;

      if (i = 0) then

    end;
    AutoSizeIndex:= 0;
    //MainColumn:= 0;
  end;

  with VST do
  begin
    TreeOptions.MiscOptions:= TreeOptions.MiscOptions
        //+ [toEditable] //делает ячейки редактируемыми
        + [toGridExtensions]
        + [toCheckSupport]
        {$IFDEF LINUX}
        + [toVariableNodeHeight]
        {$ENDIF}
        ;
    TreeOptions.SelectionOptions:= TreeOptions.SelectionOptions
        + [toExtendedFocus]
        + [toMultiSelect]
        + [toFullRowSelect]  //выбрать всю строку
        ;
    TreeOptions.PaintOptions:= TreeOptions.PaintOptions
    //+ [toUseExplorerTheme] //выделение становится "нежным" цветом
    + [toPopupMode]
    ;

    TreeOptions.AutoOptions:= TreeOptions.AutoOptions
        + [toAutoScroll]
        //+ [toAutoScrollOnExpand]
        //+ [toAutoExpand]
        + [toAutoSpanColumns]
    ;


    //CheckBox
    //CheckImageKind:= ckDarkCheck;
    CheckImageKind:= ckSystemDefault;//системная отрисовка крыжиков
    ShowHint:= True;
    HintMode:= hmTooltip;//показать обрезанное содержимое узла

  end;

  PnlObjList:= TPnlObjList.Create(True);
  EditMode:= emAdd;
  FParentID:= -1;//по умолчанию будут выбираться root-узлы

  SomeCheckBox:= TCheckBox.Create(Self);
  try
    SomeCheckBox.Parent:= Self;
    SomeCheckBox.GetPreferredSize(chbWidth, FSomeCheckBoxHeight);
  finally
    FreeAndNil(SomeCheckBox);
  end;


  {$IFDEF MSWINDOWS}
  btnRootAdd.Width:= cbbRoot.Height;
  {$ELSE}
    {$IFDEF LINUX}
    btnRootAdd.Width:= Canvas.TextWidth(btnRootAdd.Caption) + 2 * Canvas.TextWidth('H');
    {$ELSE}
    btnRootAdd.Width:= Canvas.TextWidth(btnRootAdd.Caption) + 2 * Canvas.TextWidth('0');
    {$ENDIF}
  {$ENDIF}

  btnRootEdit.Width:= btnRootAdd.Width;

  with pnlBtn do
  begin
    Constraints.MinHeight:= btnShow.BorderSpacing.Around * 2 + btnShow.Height;
    AutoSize:= True;
    Caption:= '';
    Color:= clDefault;
    BevelOuter:= bvNone;
  end;

  with pnlFrame do
  begin
    Caption:= '';
    Color:= clDefault;
    BevelOuter:= bvNone;
    Anchors:= Anchors + [akLeft];
  end;

  with scrboxFr do
  begin
    BorderStyle:= bsNone;
    Color:= clDefault;
  end;

  with pnlSettings do
  begin
    BevelOuter:= bvNone;
    Color:= clDefault;
    Caption:= '';
    AutoSize:= True;
  end;

  with pnlSearch do
  begin
    BevelOuter:= bvNone;
    Color:= clDefault;
    Caption:= '';
    AutoSize:= True;
  end;

  with rgSearchField do
  begin
    Items.Clear;
    Items.Add(CaptItemSearchNodeFieldCaption);
    Items.Add(CaptItemSearchNodeFieldDescript);
    ItemIndex:= 0;
    AutoSize:= True;
  end;

  edtSearchText.Clear;
  edtR.Clear;
  edtRR.Clear;
  edtCh.Clear;
  edtChCh.Clear;
  edtSChSCh.Clear;

  Panel1.Caption:= '';

  for i:= 0 to Pred(Panel1.ControlCount) do
    if TObject(Panel1.Controls[i]).InheritsFrom(TComboBox) then
    with TComboBox(Panel1.Controls[i]) do
    begin
      Clear;
      Style:= csDropDownList;
      Items.Add(cbbTextItemEmpty);
      Items.Add(cbbTextItemSpace);
      Items.Add(cbbTextItemLineBreak);
      ItemIndex:= 0;
    end;

  with rgCh do
  begin
    Caption:= 'Поле child-узла';
    Items.Add(cShortCapt);
    Items.Add(cLongCapt);
    ItemIndex:= 0;
    ChildSizing.VerticalSpacing:= 10;
    ChildSizing.TopBottomSpacing:= 5;
    AutoSize:= True;
  end;

  with rgSch do
  begin
    Caption:= 'Поле child-узла';
    Items.Add(cShortCapt);
    Items.Add(cLongCapt);
    ItemIndex:= 0;
    ChildSizing.VerticalSpacing:= 10;
    ChildSizing.TopBottomSpacing:= 5;
    AutoSize:= True;
  end;

  FCurUser:= '';
  FCurRole:= '';
  FIsRootHasCheckBox:= False;//нет чекбокса у root
  FIsRootTextInclude:= False;//не включаем текст root'а в итоговый текст
  FIsAllowEditNonMyOwnedEntries:= False;
  FIsAllowShowNonMyOwnedEntries:= False;
  FIsShowColumnHeader:= False;
  FIsShowPnlSearch:= False;

  FIsAllRootNodesExpand:= True;
  FIsSelectAllFoundNodes:= False;
  FIsCaseSensitiveSearch:= False;
  FLastSelectedNodeID:= -1;
  FLastSelNodeParentID:= -1;
  FLastFoundNode:= nil;
  FDepartmentKind:= '';

  //сбросим флаги
  FIsQryTreeRootUpdated:= False;
  FIsQryTreeChildUpdated:= False;

end;

end.

